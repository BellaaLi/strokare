package com.example.mytest.accelerometer;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class SensorActivity extends AppCompatActivity implements View.OnClickListener {

    private Button mstart_btn;
    private Button mstop_btn;
    private TextView mSensorTV;
    private static boolean isStart;
    String user_id;
    private TextView sensor_user_name;
    private TextView predict_text;


    private static final String TAG = "SensorActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);
        setTitle("");
        //取得user_id
        SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = preferences.getString("user_id", "none");
        GetUserData getProfile = new GetUserData();
        getProfile.execute();
        //檢查使用者是否同意過可以發送簡訊
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            // Ask for permision
            ActivityCompat.requestPermissions(this,new String[] { Manifest.permission.SEND_SMS}, 1);
        }
        else {
            // Permission has already been granted
            Log.d("permission check", "yes");
        }
        mstart_btn = findViewById(R.id.start_btn);
        mstop_btn = findViewById(R.id.stop_btn);
        mSensorTV = findViewById(R.id.sensor);
        sensor_user_name = findViewById(R.id.sensor_user_name);
        mstart_btn.setOnClickListener(this);
        mstop_btn.setOnClickListener(this);

        // 得知目前service是否開啟
        isStart = PredictService.getIsStart();

        //service傳來的message
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // 處理 Service 傳來的訊息。
                Bundle message = intent.getExtras();
                int value = message.getInt("predict");
                //中風
                if(value == 1){
                    predict_text.setTextColor(Color.parseColor("#D35858"));
                    predict_text.setText("中風!");
                }
                else{
                    predict_text.setTextColor(Color.parseColor("#6DAA61"));
                    predict_text.setText("正常");
                }

            }
        };
        final String Action = "ShowPredictResult";
        IntentFilter filter = new IntentFilter(Action);
        // 將 BroadcastReceiver 在 Activity 掛起來。
        registerReceiver(receiver, filter);
    }
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        checkStart();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.start_btn:
                Intent intent = new Intent(this, PredictService.class);
                startService(intent);
                isStart = true;
                checkStart();
                break;
            case R.id.stop_btn:
                Intent intent2 = new Intent(this, PredictService.class);
                intent2.putExtra("stop_running", "STOP");
                startService(intent2);
                isStart = false;
                checkStart();
                break;
        }
    }

    //檢查service有無被啟動過了，並使得按鈕有相應的狀態
    private void checkStart() {
        if (isStart) {
            mstart_btn.setEnabled(false);
            mstop_btn.setEnabled(true);
            mSensorTV.setText("蒐集數據中(｡･∀･)ﾉﾞ ");
        } else {
            mstart_btn.setEnabled(true);
            mstop_btn.setEnabled(false);
            mSensorTV.setText("已停止偵測!!! 請啟動手環端後，再按開始");
        }
    }

    //連線django GET 取得user_name
    private class GetUserData extends AsyncTask<Void, Void, String> {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson = "";
        //String user_id = "5";

        //private String site_url = "http://192.168.86.195:8000/userdata/" + user_id + "/";
        private String site_url = "https://strokarebackend.herokuapp.com/userdata/" + user_id + "/";

        @Override
        protected String doInBackground(Void... params) {
            try {

                URL url = new URL(site_url);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();
                Log.i(TAG, "Connect!");

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                resultJson = buffer.toString();
                Log.i("Data", resultJson);


            } catch (Exception e) {
                //postProfile();
                //PostUserData myAsyncTasks = new PostUserData();
                //myAsyncTasks.execute();
                e.printStackTrace();
                Log.e(TAG, Log.getStackTraceString(e));
            }
            return resultJson;
        }

        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);
            try {
                strJson = strJson.replace("'", "\"");
                JSONObject jsonobj = new JSONObject(strJson);

                //將所有結果透過setText由文字框印出
                String name =  jsonobj.getString("user_name");
                if(name.equals("NAN")){ //還沒輸入姓名時
                    Log.d(TAG, "onPostExecute: no name QQ");
                    sensor_user_name.setText("");
                }else {
                    sensor_user_name.setText(name+"！");
                }
            } catch (JSONException e) {

                e.printStackTrace();
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }

    }
}