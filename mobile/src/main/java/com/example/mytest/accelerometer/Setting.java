package com.example.mytest.accelerometer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.DataType;

public class Setting extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "Setting";

    private int GOOGLE_FIT_PERMISSIONS_REQUEST_CODE = 1;
    FitnessOptions fitnessOptions;
    GoogleSignInAccount account;

    private boolean login;

    private Button logoutBtn;
    private Button updateProfileBtn;
    private Button mGoogleBtn;
    private Button mChangepasswordBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        logoutBtn = findViewById(R.id.logout_btn);
        updateProfileBtn = findViewById(R.id.update_profile_btn);
        mGoogleBtn = findViewById(R.id.google_btn);
        mChangepasswordBtn = findViewById(R.id.changepassword_btn);

        logoutBtn.setOnClickListener(this);
        updateProfileBtn.setOnClickListener(this);
        mGoogleBtn.setOnClickListener(this);
        mChangepasswordBtn.setOnClickListener(this);

        setFitnessOption();
        checkFitInstalled();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.logout_btn:
                //登出
                logout();
                break;
            case R.id.update_profile_btn:
                //修改個人資料
                update_profile();
                break;
            case R.id.google_btn:
                //修改個人資料
                google_login();
                break;
            case R.id.changepassword_btn:
                //修改密碼
                change_password();
        }
    }

    //登出
    private void logout(){
        SharedPreferences preferences = getSharedPreferences("login", MODE_PRIVATE);
        preferences.edit()
                .remove("user_id")
                .remove("token")
                .remove("loggedin_state")
                .remove("username")
                .commit();
        //回登入畫面
        Intent it = new Intent();
        it.setClass(Setting.this, MainActivity.class);
        startActivity(it);
    }

    //修改個人資料
    private void update_profile(){
        //到個人資料頁面
        Intent it = new Intent();
        it.setClass(Setting.this, UserProfile.class);
        startActivity(it);
    }

    //google登入
    private void google_login(){
        if (!login) {
            // Get an instance of the Account object to use with the API
            GoogleSignIn.requestPermissions(
                    this,
                    GOOGLE_FIT_PERMISSIONS_REQUEST_CODE,
                    account,
                    fitnessOptions);


            Log.i(TAG, "google認證" );
        }
        else{
            GoogleSignInOptions gso = new GoogleSignInOptions.
                    Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
                    build();

            GoogleSignInClient googleSignInClient=GoogleSignIn.getClient(this,gso);
            googleSignInClient.signOut();

            new AlertDialog.Builder(this)
                    .setTitle("google登出成功")
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .create()
                    .show();

            Log.i(TAG, "google登出" );
            mGoogleBtn.setText("google認證");
            login = false;
        }
    }

    // ask for permission
    private void setFitnessOption() {
        fitnessOptions =
                FitnessOptions.builder()
                        .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.AGGREGATE_DISTANCE_DELTA, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.AGGREGATE_CALORIES_EXPENDED, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.TYPE_HEART_RATE_BPM, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.TYPE_SLEEP_SEGMENT, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.TYPE_ACTIVITY_SEGMENT, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.AGGREGATE_ACTIVITY_SUMMARY, FitnessOptions.ACCESS_READ)
                        .build();

        account = GoogleSignIn.getAccountForExtension(this, fitnessOptions);
    }

    // Check if the user has previously granted the necessary data access, and if not, initiate the authorization flow
    private boolean checkFitInstalled() {
        // 如果尚未登入過
        if (! GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions)) {
            login = false;
            mGoogleBtn.setText("google認證");
        }
        else{
            login = true;
            mGoogleBtn.setText("google登出");

        }

        return login;
    }

    // GoogleSignIn.requestPermissions的結果
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        super.onActivityResult(requestCode, resultCode, intent);
        if(resultCode == RESULT_OK) {
            if(requestCode == GOOGLE_FIT_PERMISSIONS_REQUEST_CODE) {
                Log.i(TAG, "GoogleSignIn 成功" );

                new AlertDialog.Builder(this)
                        .setTitle("google帳號認證成功")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .create()
                        .show();

                mGoogleBtn.setText("google登出");
                login = true;
            }
            else{
                Log.i(TAG, "Result wasn't from Google Fit" );
            }
        }
        else {
            Log.i(TAG, "Permission not granted" );
        }
    }

    //修改密碼
    private void change_password(){
        //到修改密碼畫面
        Intent it = new Intent();
        it.setClass(Setting.this, ChangePasswordActivity.class);
        startActivity(it);
    }
}