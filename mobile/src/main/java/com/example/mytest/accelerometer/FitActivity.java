package com.example.mytest.accelerometer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessActivities;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Goal;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.request.GoalsReadRequest;
import com.google.android.gms.fitness.request.SessionReadRequest;


import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class FitActivity extends AppCompatActivity {
    private static final String TAG = "FitActivity";

    private int GOOGLE_FIT_PERMISSIONS_REQUEST_CODE = 1;
    FitnessOptions fitnessOptions;
    GoogleSignInAccount account;

    private TextView mCountTv;
    private TextView mGoalTv;
    private TextView mHeartTv;
    private TextView mHeartTimeTv;
    private TextView mActivityTv;
    private TextView mActTimeTv;
    private Button mInfoStepBtn;
    private Button mInfoHeartBtn;
    private Button mInfoActivityBtn;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fit);
        mCountTv = (TextView) findViewById(R.id.step_count);
        mGoalTv = (TextView) findViewById(R.id.step_goal);
        mHeartTv= (TextView) findViewById(R.id.heart_rate);
        mHeartTimeTv= (TextView) findViewById(R.id.heart_time);
        mActivityTv= (TextView) findViewById(R.id.activity);
        mActTimeTv= (TextView) findViewById(R.id.activity_time);
        mInfoStepBtn = (Button) findViewById(R.id.info_step);
        mInfoHeartBtn = (Button) findViewById(R.id.info_heart);
        mInfoActivityBtn = (Button) findViewById(R.id.info_activity);
        mProgressBar = (ProgressBar)findViewById(R.id.progressBar);

        isPkgInstalled();
        setFitnessOption();
        checkFitInstalled();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onResume() {
        super.onResume();
        isPkgInstalled();
        checkFitInstalled();

        //獲取步數
        getTodayStep();
        //獲取心率
        getHeartRate();
        //獲取活動
        getActivity();

        // 設置按下[查看更多]按鈕後的動作(跳到StepTabActivity)
        mInfoStepBtn.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setClass(this , StepTabActivity.class);
            startActivity(intent);
        });

        // 設置按下[查看更多]按鈕後的動作(跳到HeartMoreActivity)
        mInfoHeartBtn.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setClass(this , HeartMoreActivity.class);
            startActivity(intent);
        });

        // 設置按下[查看更多]按鈕後的動作(跳到ActMoreActivity)
        mInfoActivityBtn.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setClass(FitActivity.this , ActMoreActivity.class);
            startActivity(intent);
        });

    }


    // ask for permission
    private void setFitnessOption() {
        fitnessOptions =
                FitnessOptions.builder()
                        .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.AGGREGATE_DISTANCE_DELTA, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.AGGREGATE_CALORIES_EXPENDED, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.TYPE_HEART_RATE_BPM, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.TYPE_SLEEP_SEGMENT, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.TYPE_ACTIVITY_SEGMENT, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.AGGREGATE_ACTIVITY_SUMMARY, FitnessOptions.ACCESS_READ)
                        .build();

        account = GoogleSignIn.getAccountForExtension(this, fitnessOptions);
    }

    //判斷Google Fit有無安裝
    private void isPkgInstalled() {
        PackageInfo packageInfo = null;

        try {
            packageInfo = this.getPackageManager().getPackageInfo("com.google.android.apps.fitness", 0);
        } catch (PackageManager.NameNotFoundException e) {
            packageInfo = null;
            e.printStackTrace();
        }

        Log.i(TAG, ""+packageInfo);

        if (packageInfo == null) {
            new AlertDialog.Builder(this)
                    .setTitle("此功能需先下載Google Fit才能使用")
                    .setPositiveButton("前往下載", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // 前往google pay
                            Uri uri = Uri.parse("market://details?id=" + "com.google.android.apps.fitness");
                            Intent intent = new Intent(Intent.ACTION_VIEW,uri);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("返回首頁", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(FitActivity.this, HomeActivity.class);
                            startActivity(intent);
                        }
                    })
                    .setCancelable(false)
                    .create()
                    .show();
        }
    }

    // Check if the user has previously granted the necessary data access, and if not, initiate the authorization flow
    private void checkFitInstalled() {
        // 如果尚未登入過
        if (! GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions)) {
            new AlertDialog.Builder(this)
                    .setTitle("尚未google認證，無法查看我的數據")
                    .setPositiveButton("前往google認證", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Get an instance of the Account object to use with the API
                            GoogleSignIn.requestPermissions(
                                    FitActivity.this,
                                    GOOGLE_FIT_PERMISSIONS_REQUEST_CODE,
                                    account,
                                    fitnessOptions);
                            Log.i(TAG, "google登入" );
                        }
                    })
                    .setNegativeButton("返回首頁", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(FitActivity.this, HomeActivity.class);
                            startActivity(intent);
                        }
                    })
                    .setCancelable(false)
                    .create()
                    .show();
        }
    }

    // GoogleSignIn.requestPermissions的結果
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        super.onActivityResult(requestCode, resultCode, intent);
        if(resultCode == RESULT_OK) {
            if(requestCode == GOOGLE_FIT_PERMISSIONS_REQUEST_CODE) {
                Log.i(TAG, "GoogleSignIn 成功" );
                checkFitInstalled();
            }
            else{
                Log.i(TAG, "Result wasn't from Google Fit" );
            }
        }
        else {
            Log.i(TAG, "Permission not granted" );
        }
    }


    // 查看當天步數
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getTodayStep() {
        //設定步數時間範圍
        ZonedDateTime startTime = LocalDate.now().atStartOfDay(ZoneId.systemDefault());
        ZonedDateTime endTime = LocalDateTime.now().atZone(ZoneId.systemDefault());
        //ZonedDateTime startTime = LocalDate.of(2021,9,18).atStartOfDay(ZoneId.systemDefault());
        //ZonedDateTime endTime = LocalDateTime.of(2021,9,18,23,59,59).atZone(ZoneId.systemDefault());
        Log.i("STEP", "Range Start: " + startTime);
        Log.i("STEP", "Range End: " + endTime);

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(DataType.AGGREGATE_STEP_COUNT_DELTA)
                .bucketByTime(1, TimeUnit.DAYS)
                .enableServerQueries()
                .setTimeRange(startTime.toEpochSecond(), endTime.toEpochSecond(), TimeUnit.SECONDS)
                .build();

        Fitness.getHistoryClient(this, account)
                .readData(readRequest)
                .addOnSuccessListener (response -> {
                    int total=0;
                    // The aggregate query puts datasets into buckets, so convert to a single list of datasets
                    for (Bucket bucket : response.getBuckets()) {
                        for (DataSet dataSet : bucket.getDataSets()) {
                            for (DataPoint dp : dataSet.getDataPoints()) {
                                for (Field field : dp.getDataType().getFields()) {
                                    total += dp.getValue(field).asInt();

                                    Log.e("History", "\tField: " + field.getName() +
                                            " Value: " + dp.getValue(field));
                                }
                            }
                        }
                    }
                    mCountTv.setText(""+total);

                    readGoals(total);
                })
                .addOnFailureListener(e ->
                        Log.w("STEP", "There was an error reading data from Google Fit", e));
    }

    // 讀取步數目標，並顯示剩餘步數在螢幕上
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void readGoals(int total) {
        GoalsReadRequest goalsReadRequest = new GoalsReadRequest.Builder()
                .addDataType(DataType.TYPE_STEP_COUNT_DELTA)
                .build();

        Fitness.getGoalsClient(getApplicationContext(), account)
                .readCurrentGoals(goalsReadRequest)
                .addOnSuccessListener(goals -> {
                    Optional<Goal> optionalGoal = goals.stream().findFirst();
                    int goal;
                    if(optionalGoal.isPresent()){
                        goal = (int)optionalGoal.get().getMetricObjective().getValue();
                    }
                    else{
                        goal = -1;
                    }

                    //設定進度條
                    mProgressBar.setMax(goal);

                    int remaining=(goal-total);
                    if(goal<0){
                        mGoalTv.setText("目標資料有誤，暫時無法取得");
                    }
                    else if(remaining>0){
                        mGoalTv.setText("距離目標還有"+remaining+"步!");
                        mProgressBar.setProgress(total);
                    }
                    else{
                        mGoalTv.setText("已達標!太強了(❁´◡`❁)");
                        mProgressBar.setProgress(goal);
                    }
                });
    }

    // 查看今日心率
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getHeartRate() {
        //設定時間範圍
        ZonedDateTime startTime = LocalDate.now().atStartOfDay(ZoneId.systemDefault());
        ZonedDateTime endTime = LocalDateTime.now().atZone(ZoneId.systemDefault());
        //ZonedDateTime startTime = LocalDate.of(2021,9,18).atStartOfDay(ZoneId.systemDefault());
        //ZonedDateTime endTime = LocalDateTime.of(2021,9,18,23,59,59).atZone(ZoneId.systemDefault());
        Log.i("HEART", "Range Start: " + startTime);
        Log.i("HEART", "Range End: " + endTime);

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .read(DataType.TYPE_HEART_RATE_BPM)
                .bucketByTime(1, TimeUnit.DAYS)
                .enableServerQueries()
                .setTimeRange(startTime.toEpochSecond(), endTime.toEpochSecond(), TimeUnit.SECONDS)
                .build();

        Fitness.getHistoryClient(this, account)
                .readData(readRequest)
                .addOnSuccessListener (response -> {
                    // The aggregate query puts datasets into buckets, so convert to a single list of datasets
                    for (Bucket bucket : response.getBuckets()) {
                        for (DataSet dataSet : bucket.getDataSets()) {
                            for (DataPoint dp : dataSet.getDataPoints()) {
                                for (Field field : dp.getDataType().getFields()) {
                                    float heart_rate = dp.getValue(field).asFloat();

                                    //通過Spannable對象設置textview的樣式
                                    SpannableStringBuilder builder = new SpannableStringBuilder();
                                    SpannableString str1= new SpannableString(""+(int)heart_rate);
                                    builder.append(str1);
                                    SpannableString str2= new SpannableString("  bpm");
                                    str2.setSpan(new ForegroundColorSpan(Color.GRAY), 0, str2.length(), 0);
                                    str2.setSpan(new AbsoluteSizeSpan(12,true), 0, str2.length(), 0);
                                    builder.append(str2);

                                    mHeartTv.setText( builder, TextView.BufferType.SPANNABLE);

                                    //mHeartTv.setText(""+(int)heart_rate);
                                    SimpleDateFormat dtf = new SimpleDateFormat("M/d HH:mm");
                                    mHeartTimeTv.setText(dtf.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                                    Log.e("History", "\tField: " + field.getName() +
                                            " Value: " + dp.getValue(field));
                                }
                            }
                        }
                    }
                })
                .addOnFailureListener(e ->
                        Log.w("HEART", "There was an error reading data from Google Fit", e));
    }

    // 查看今日活動
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getActivity() {
        //設定時間範圍
        ZonedDateTime startTime = LocalDate.now().atStartOfDay(ZoneId.systemDefault());
        ZonedDateTime endTime = LocalDateTime.now().atZone(ZoneId.systemDefault());
        //ZonedDateTime startTime = LocalDate.of(2021,9,18).atStartOfDay(ZoneId.systemDefault());
        //ZonedDateTime endTime = LocalDateTime.of(2021,9,18,23,59,59).atZone(ZoneId.systemDefault());
        Log.i("ACT", "Range Start: " + startTime);
        Log.i("ACT", "Range End: " + endTime);

        SessionReadRequest readRequest = new SessionReadRequest.Builder()
                .read(DataType.TYPE_ACTIVITY_SEGMENT)
                .readSessionsFromAllApps()
                .enableServerQueries()
                .setTimeInterval(startTime.toEpochSecond(), endTime.toEpochSecond(), TimeUnit.SECONDS)
                .build();
        Fitness.getSessionsClient(this, account)
                .readSession(readRequest)
                .addOnSuccessListener (response -> {
                    ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mActTimeTv.getLayoutParams();
                    SimpleDateFormat dtf = new SimpleDateFormat("HH:mm");
                    Boolean hasActivity = false;

                    for (Session session : response.getSessions()) {
                        mActivityTv.setText(""+session.getName());
                        String time = secondToTime(session.getActiveTime(TimeUnit.SECONDS));
                        String start_time = dtf.format(session.getStartTime(TimeUnit.MILLISECONDS));
                        String end_time = dtf.format(session.getStartTime(TimeUnit.MILLISECONDS));
                        mActTimeTv.setText(start_time + "~" + end_time +"\n總長:" + time);
                        hasActivity=true;
                    }
                    if(!hasActivity){
                        layoutParams.setMargins(70, 0, 0, 0);
                        mActTimeTv.setLayoutParams(layoutParams);
                        mActivityTv.setLayoutParams(layoutParams);

                    }
                })
                .addOnFailureListener(e ->
                        Log.w("ACT", "There was an error reading data from Google Fit", e));
    }

    //秒數轉時間
    private String secondToTime(long second){
        long days = second / 86400;            //轉換天數
        second = second % 86400;            //剩余秒數
        long hours = second / 3600;            //轉換小時
        second = second % 3600;                //剩余秒數
        long minutes = second /60;            //轉換分鍾
        second = second % 60;                //剩余秒數
        if(days>0){
            return days + "天" + hours + "小時" + minutes + "分" + second + "秒";
        }else if(hours>0){
            return hours + "小時" + minutes + "分" + second + "秒";
        }else{
            return minutes + "分" + second + "秒";
        }
    }

}