package com.example.mytest.accelerometer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.mytest.accelerometer.util.ExpandLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class SelectionModeActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "SelectionMode";
    private TextView tv_h_3X3, tv_h_5X5, tv_h_7X7, tv_h_9X9, tv_prime_5X5, tv_odd_3X3, tv_odd_5X5, tv_odd_7X7;
    private Button tv_normal, tv_prime_number,tv_odd_number;
    private LinearLayout rl_left_layout;
    //private LinearLayout ll_my_history;
    //private ImageView history_cancel;
    //private ScrollView sl_introduce;
    private TextView iv_menu;
    private ExpandLayout el_normal, el_prime_number, el_odd_number, color_select;
    private TextView tv_mode_3, tv_mode_5, tv_mode_7, tv_mode_9, tv_start, tv_history, tv_introduce;
    private TextView tv_delete_All_data;
    private TextView tv_prime_number_5, tv_odd_number_3, tv_odd_number_5, tv_odd_number_7;
    private int type = 5;
    private int countdown = 4;
    private DrawerLayout drawerLayout;
    private SharedPreferences.Editor editor;
    private SharedPreferences prefData_type;
    private SharedPreferences preferences;
    private TextView tv_set_color_mode;
    private RadioGroup color_rg;
    private RadioButton color_on, color_off;
    Map<Integer, Boolean> map;
    private static boolean isExit = false;
    private static final String Normal_Mode = "normalMode";
    private static final String Prime_Number_Mode = "PrimeNumberMode";
    private static final String Odd_Number_Mode = "OddNumberMode";
    private static final String Color_Mode = "ColorMode";
    private String Sudoku_mode = "";
    private boolean whetherColor = false;
    private String user_id;
    int flag = 0;
    int flag_nor = 0;
    int flag_prime = 0;
    int flag_odd = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection_mode);

        //历史的存储
        //preferences = getSharedPreferences("history_data", MODE_PRIVATE);

        //取得user_id
        SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = preferences.getString("user_id", "none");


        //实例化和获取 本地的 data-type type表示上一次选择的模式
        editor = getSharedPreferences("data-type", MODE_PRIVATE).edit();
        prefData_type = getSharedPreferences("data-type", MODE_PRIVATE);//类型和个数
        whetherColor = prefData_type.getBoolean("whether_Color", false);
        type = prefData_type.getInt("type", 0);
        Sudoku_mode = prefData_type.getString("mode_type", "");
        Log.d(TAG, "onCreate: color："+whetherColor);
        if (type == 0 && Sudoku_mode.isEmpty()) {
            type = 5;
            Sudoku_mode = Normal_Mode;
        }

        //將xml的元件放入宣告的變數中
        initView();
        setView();
    }

    private void initView() {

        //activity_selection_mode.xml//
        //整個主畫面(包含選單)
        drawerLayout = findViewById(R.id.dl_deaLayout);
        //開始(主畫面)
        tv_start = findViewById(R.id.tv_start);
        //選單(主畫面)
        iv_menu = findViewById(R.id.iv_menu);
        //選單畫面(選單中)
        rl_left_layout = findViewById(R.id.rl_left_layout);
        //普通模式(選單中)
        tv_normal = findViewById(R.id.tv_normal);
        //質數模式(選單中)
        tv_prime_number = findViewById(R.id.tv_prime_number);
        //奇數模式(選單中)
        tv_odd_number = findViewById(R.id.tv_odd_number);
        //最快紀錄
        tv_history = findViewById(R.id.tv_history);
        //history_cancel = findViewById(R.id.history_cancel);
        //最快紀錄的畫面(在activity_my_history.xml)
        //ll_my_history = findViewById(R.id.ll_my_history);
        //彩色模式
        color_rg = findViewById(R.id.color_radio_group);
        color_on = findViewById(R.id.color_on);
        color_off = findViewById(R.id.color_off);
        //刪除紀錄
        tv_delete_All_data = findViewById(R.id.tv_delete_All_data);


        //normal_mode.xml//
        el_normal = findViewById(R.id.el_normal);
        //普通模式的選項(在normal_mode.xml)
        tv_mode_3 = findViewById(R.id.tv_mode_3);
        tv_mode_5 = findViewById(R.id.tv_mode_5);
        tv_mode_7 = findViewById(R.id.tv_mode_7);
        //tv_mode_9 = findViewById(R.id.tv_mode_9);


        //prime_number_mode.xml//
        el_prime_number = findViewById(R.id.el_prime_number);
        //質數模式的選項(在prime_number_mode.xml)
        tv_prime_number_5 = findViewById(R.id.tv_prime_number_5);


        //odd_number_mode.xml//
        el_odd_number = findViewById(R.id.el_odd_number);
        //奇數模式的選項(在odd_number_mode.xml)
        tv_odd_number_7 = findViewById(R.id.tv_odd_number_7);
        tv_odd_number_5 = findViewById(R.id.tv_odd_number_5);
        tv_odd_number_3 = findViewById(R.id.tv_odd_number_3);


        //設定方格
        //質數模式的方格
        tv_prime_5X5 = findViewById(R.id.tv_prime_5X5);
        //機數模式的方格
        tv_odd_3X3 = findViewById(R.id.tv_odd_3X3);
        tv_odd_5X5 = findViewById(R.id.tv_odd_5X5);
        tv_odd_7X7 = findViewById(R.id.tv_odd_7X7);
        //普通模式的方格
        tv_h_3X3 = findViewById(R.id.tv_h_3X3);
        tv_h_5X5 = findViewById(R.id.tv_h_5X5);
        tv_h_7X7 = findViewById(R.id.tv_h_7X7);
        //tv_h_9X9 = findViewById(R.id.tv_h_9X9);
    }

    private void setView() {
        tv_start.setOnClickListener(this);
        iv_menu.setOnClickListener(this);

        tv_normal.setOnClickListener(this);
        tv_prime_number.setOnClickListener(this);
        tv_odd_number.setOnClickListener(this);
        //普通
        tv_mode_3.setOnClickListener(this);
        tv_mode_5.setOnClickListener(this);
        tv_mode_7.setOnClickListener(this);
        //tv_mode_9.setOnClickListener(this);
        //質數
        tv_prime_number_5.setOnClickListener(this);
        //奇數
        tv_odd_number_7.setOnClickListener(this);
        tv_odd_number_5.setOnClickListener(this);
        tv_odd_number_3.setOnClickListener(this);

        //其他
        tv_history.setOnClickListener(this);
        //history_cancel.setOnClickListener(this);
        //tv_set_color_mode.setOnClickListener(this);
        tv_delete_All_data.setOnClickListener(this);

        el_normal.initExpand(false);
        el_prime_number.initExpand(false);
        el_odd_number.initExpand(false);
        if(whetherColor){
            color_on.setChecked(true);
        }else{
            color_off.setChecked(true);
        }
    }


    //按下後的反應
    @Override
    public void onClick(View view) {
        //if(whetherColor){

        //}else{

       // }
        switch (view.getId()) {
            case R.id.tv_mode_3:
                tv_mode_3.setActivated(true);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED); //在倒数的时候，禁止侧边滑动
                handler.sendMessageDelayed(handler.obtainMessage(1), 0);
                type = 3;
                Sudoku_mode = Normal_Mode;
                break;
            case R.id.tv_mode_5:
                tv_mode_5.setActivated(true);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                handler.sendMessageDelayed(handler.obtainMessage(1), 0);
                type = 5;
                Sudoku_mode = Normal_Mode;
                break;
            case R.id.tv_mode_7:
                tv_mode_7.setActivated(true);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                handler.sendMessageDelayed(handler.obtainMessage(1), 0);
                type = 7;
                Sudoku_mode = Normal_Mode;
                break;
            /*case R.id.tv_mode_9:
                tv_mode_9.setActivated(true);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                handler.sendMessageDelayed(handler.obtainMessage(1), 0);
                type = 9;
                Sudoku_mode = Normal_Mode;
                break;*/
            case R.id.tv_prime_number_5:
                tv_prime_number_5.setActivated(true);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                handler.sendMessageDelayed(handler.obtainMessage(1), 0);
                type = 5;
                Sudoku_mode = Prime_Number_Mode;
                break;
            case R.id.tv_odd_number_3:
                tv_odd_number_3.setActivated(true);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                handler.sendMessageDelayed(handler.obtainMessage(1), 0);
                type = 3;
                Sudoku_mode = Odd_Number_Mode;
                break;
            case R.id.tv_odd_number_5:
                tv_odd_number_5.setActivated(true);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                handler.sendMessageDelayed(handler.obtainMessage(1), 0);
                type = 5;
                Sudoku_mode = Odd_Number_Mode;
                break;
            case R.id.tv_odd_number_7:
                tv_odd_number.setActivated(true);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                handler.sendMessageDelayed(handler.obtainMessage(1), 0);
                type = 7;
                Sudoku_mode = Odd_Number_Mode;
                break;
            //case R.id.tv_start:
                //drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                //handler.sendMessageDelayed(handler.obtainMessage(1), 0);
                //break;
            case R.id.tv_history:
                //drawerLayout.closeDrawers();//关闭侧滑
                Intent intent = new Intent(getApplicationContext(), HistoryActivity.class);
                startActivity(intent);

                break;
            case R.id.iv_menu:
                drawerLayout.openDrawer(rl_left_layout);//打开侧滑
                break;
            case R.id.tv_normal:
                flag = 1;
                el_normal.expand();
                el_odd_number.collapse();
                el_prime_number.collapse();
                Log.d(TAG, "normal");
                break;
            case R.id.tv_prime_number:
                flag = 2;
                el_prime_number.expand();
                el_normal.collapse();
                el_odd_number.collapse();
                Log.d(TAG, "prime");
                break;
            case R.id.tv_odd_number:
                flag = 3;
                el_odd_number.expand();
                el_normal.collapse();
                el_prime_number.collapse();
                Log.d(TAG, "odd");
                break;
            case R.id.tv_delete_All_data:
                new AlertDialog.Builder(SelectionModeActivity.this)
                        .setMessage("提示：\n" + "將會刪除原本的紀錄，所有模式紀錄都將歸零。")
                        .setPositiveButton("刪除紀錄", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                SelectionModeActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        editor.clear().commit();

                                        //preferences.edit().clear().commit();

                                        //刪除紀錄
                                        deleteHistoryData();

                                    }
                                });
                            }
                        })
                        .setNegativeButton("返回", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        })
                        .show();
                break;
        }
        switch (flag){
            case 1:
                tv_normal.setActivated(true);
                tv_prime_number.setActivated(false);
                tv_odd_number.setActivated(false);
                break;
            case 2:
                tv_normal.setActivated(false);
                tv_prime_number.setActivated(true);
                tv_odd_number.setActivated(false);
                break;
            case 3:
                tv_normal.setActivated(false);
                tv_prime_number.setActivated(false);
                tv_odd_number.setActivated(true);
                break;
        }


        //彩色模式
        switch (color_rg.getCheckedRadioButtonId()){
            case R.id.color_on:
                whetherColor = true;
                editor.putBoolean("whether_Color",whetherColor);
                //editor.apply();
                //Toast.makeText(this, "開啟彩色模式", Toast.LENGTH_LONG).show();
                //Log.d(TAG, "rg開啟色彩模式");
                break;
            case R.id.color_off:
                whetherColor = false;
                editor.putBoolean("whether_Color",whetherColor);
                //editor.apply();
                //Toast.makeText(this, "關閉彩色模式", Toast.LENGTH_LONG).show();
                //Log.d(TAG, "rg關閉色彩模式");
                break;
        }
        editor.putInt("type", type);
        editor.putString("mode_type", Sudoku_mode);
        editor.apply();
    }

    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    //sl_introduce.setVisibility(View.GONE);
                    //ll_my_history.setVisibility(View.GONE);
                    tv_start.setOnClickListener(null);
                    iv_menu.setOnClickListener(null);
                    countdown--;
                    if (countdown > 0) {
                        tv_start.setText("" + countdown);
                        handler.sendMessageDelayed(handler.obtainMessage(1), 800);
                    } else {
                        Intent intentStart = new Intent(SelectionModeActivity.this, ClickInterfaceActivity.class);
                        intentStart.putExtra("tv_mode", type);
                        intentStart.putExtra("Sudoku_mode", Sudoku_mode);
                        intentStart.putExtra("whetherColor", whetherColor);
                        startActivity(intentStart);
                        countdown = 4;
                        tv_start.setOnClickListener(SelectionModeActivity.this);
                        iv_menu.setOnClickListener(SelectionModeActivity.this);
                    }
                    break;
            }
            super.handleMessage(msg);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        //允许侧边滑动
        if (drawerLayout != null) {
            //tv_start.setText("點我開始");
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    //键盘返回键监听
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    //处理键盘事件
    private void exit() {
        if (!isExit) {
            //ll_my_history.setVisibility(View.GONE);
            isExit = true;
            Toast.makeText(SelectionModeActivity.this, "再按一次退出專注力訓練", Toast.LENGTH_SHORT).show();
            mHandler.sendEmptyMessageDelayed(0, 2000);
        } else {
            Intent intent = new Intent(getApplicationContext(), PreventActivity.class);
            startActivity(intent);
        }
    }

    @SuppressLint("HandlerLeak")
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            isExit = false;
        }
    };


    private void deleteHistoryData(){
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(1000000, TimeUnit.SECONDS)   // 設置連線Timeout
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
                .build();

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("normalMode_data_time3", "暫無紀錄");
            jsonObject.put("normalMode_data_time5", "暫無紀錄");
            jsonObject.put("normalMode_data_time7", "暫無紀錄");
            jsonObject.put("normalMode_data_time9", "暫無紀錄");
            jsonObject.put("PrimeNumberMode_data_time5", "暫無紀錄");
            jsonObject.put("OddNumberMode_data_time3", "暫無紀錄");
            jsonObject.put("OddNumberMode_data_time5", "暫無紀錄");
            jsonObject.put("OddNumberMode_data_time7", "暫無紀錄");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        // put your json here
        RequestBody formBody = RequestBody.create(JSON, jsonObject.toString());

        //get連線
        Request request = new Request.Builder()
                .url("https://strokarebackend.herokuapp.com/focushistory/"+user_id+"/")
                //.header("Authorization", "Token "+token)
                .put(formBody)
                .build();

        // 建立Call
        Call call = client.newCall(request);
        // 執行Call連線到網址
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 連線成功
                String result = response.body().string();

                try {
                    JSONObject Jobject = new JSONObject(result);
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getApplicationContext(), "紀錄刪除成功", Toast.LENGTH_SHORT).show();
                        }
                    });


                } catch (JSONException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getApplicationContext(), "error :(", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }

            @Override
            public void onFailure(Call call, IOException e) {
                // 連線失敗
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), "連線失敗，請稍後再試", Toast.LENGTH_SHORT).show();
                    }
                });





            }
        });
    }



}