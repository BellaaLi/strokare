package com.example.mytest.accelerometer;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.material.card.MaterialCardView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HomeActivity extends AppCompatActivity {
    private static final String TAG = "HomeActivity";
    //主畫面
    MaterialCardView my_data;
    MaterialCardView prevent;
    MaterialCardView sensor;
    MaterialCardView setup;
    private String user_id;
    private TextView home_user_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setTitle("");
        //取得user_id
        SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = preferences.getString("user_id", "none");
        Log.d(TAG,"user_id："+ user_id);
        home_user_name = findViewById(R.id.home_user_name);
        GetUserData getProfile = new GetUserData();
        getProfile.execute();
    }

    @Override
    protected void onStart() {
        super.onStart();

        my_data = (MaterialCardView) findViewById(R.id.mydata_card);
        prevent = (MaterialCardView) findViewById(R.id.prevent_card);
        sensor = (MaterialCardView) findViewById(R.id.predict_card);
        setup = (MaterialCardView) findViewById(R.id.setting_card);



        my_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), FitActivity.class);
                startActivity(intent);
            }
        });

        prevent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), PreventActivity.class);
                startActivity(intent);

            }
        });

        sensor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SensorActivity.class);
                startActivity(intent);
            }
        });

        setup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Setting.class);
                startActivity(intent);

            }
        });
    }

    //連線django GET 取得user_name
    private class GetUserData extends AsyncTask<Void, Void, String> {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson = "";
        //String user_id = "5";

        //private String site_url = "http://192.168.86.195:8000/userdata/" + user_id + "/";
        private String site_url = "https://strokarebackend.herokuapp.com/userdata/" + user_id + "/";

        @Override
        protected String doInBackground(Void... params) {
            try {

                URL url = new URL(site_url);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();
                Log.i(TAG, "Connect!");

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                resultJson = buffer.toString();
                Log.i("Data", resultJson);


            } catch (Exception e) {
                //postProfile();
                //PostUserData myAsyncTasks = new PostUserData();
                //myAsyncTasks.execute();
                e.printStackTrace();
                Log.e(TAG, Log.getStackTraceString(e));
            }
            return resultJson;
        }

        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);
            try {
                strJson = strJson.replace("'", "\"");
                JSONObject jsonobj = new JSONObject(strJson);

                //將所有結果透過setText由文字框印出
                String name =  jsonobj.getString("user_name");
                if(name.equals("NAN")){ //還沒輸入姓名時
                    Log.d(TAG, "onPostExecute: no name QQ");
                    home_user_name.setText("");
                }else {
                    home_user_name.setText("嗨~ "+name+" 您好！");
                }
            } catch (JSONException e) {

                e.printStackTrace();
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }

    }
}