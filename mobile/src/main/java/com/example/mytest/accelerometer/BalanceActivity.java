package com.example.mytest.accelerometer;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.mytest.accelerometer.util.ExpandLayout;

public class BalanceActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "Balance";
    //三個難度(選單中)
    private Button bal_elementary, bal_advance, bal_highlevel;
    //初階篇動作畫面(選單中)
    private TextView elementary_tiptoe, elementary_stride;
    //進階篇動作畫面(選單中)
    private TextView advance_tiptoe, advance_stride, advance_straight;
    //高階篇動作畫面(選單中)
    private TextView highlevel_stand, highlevel_walk,  highlevel_ball;
    //開始(主畫面)
    //private TextView bal_start;
    //選單(主畫面)
    private TextView bal_menu;
    //選單畫面(選單中)
    private LinearLayout bal_left_layout;

    //三個難度的所有選項(在bal_elementary.xml, bal_advance.xml, bal_highlevel.xml中)
    private ExpandLayout el_elementary, el_advance, el_highlevel;

    //整個主畫面(包含選單)
    private DrawerLayout drawerLayout;
    private Spinner spinner_elementary, spinner_advance, spinner_high;

    private static boolean isExit = false;
    int flag = 0;
    int flag_ele = 0;
    int flag_adv = 0;
    int flag_high = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance);

        initView();
        setView();
    }

    private void initView() {
        //整個主畫面(包含選單)
        drawerLayout = findViewById(R.id.bal_deaLayout);
        //開始(主畫面)
        //bal_start = findViewById(R.id.bal_start);
        //選單(主畫面)
        bal_menu = findViewById(R.id.bal_menu);

        //選單畫面(選單中)
        bal_left_layout = findViewById(R.id.bal_left_layout);

        //三個難度(選單中)
        bal_advance = findViewById(R.id.bal_advance);
        bal_elementary = findViewById(R.id.bal_elementary);
        bal_highlevel = findViewById(R.id.bal_highlevel);
        //初階篇動作畫面(選單中)
        elementary_tiptoe = findViewById(R.id.elementary_tiptoe);
        elementary_stride = findViewById(R.id.elementary_stride);
        //進階篇動作畫面(選單中)
        advance_stride = findViewById(R.id.advance_stride);
        advance_tiptoe = findViewById(R.id.advance_tiptoe);
        advance_straight = findViewById(R.id.advance_straight);
        //高階篇動作畫面(選單中)
        highlevel_stand = findViewById(R.id.highlevel_stand);
        highlevel_walk = findViewById(R.id.highlevel_walk);
        highlevel_ball = findViewById(R.id.highlevel_ball);


        //三個難度(在各自的xml)
        el_advance = findViewById(R.id.el_advance);
        el_elementary = findViewById(R.id.el_elementary);
        el_highlevel = findViewById(R.id.el_highlevel);
        //下拉是選單
        //spinner_advance = findViewById(R.id.spinnerAdvance);
        //spinner_elementary = findViewById(R.id.spinnerElementary);
        //spinner_high = findViewById(R.id.spinnerHighlevel);
    }

    private void setView() {
        //bal_start.setOnClickListener(this);
        //選單(主畫面)
        bal_menu.setOnClickListener(this);

        //三個難度(選單中)
        bal_elementary.setOnClickListener(this);
        bal_advance.setOnClickListener(this);
        bal_highlevel.setOnClickListener(this);
        //初階篇動作畫面(選單中)
        elementary_tiptoe.setOnClickListener(this);
        elementary_stride.setOnClickListener(this);
        //進階篇動作畫面(選單中)
        advance_stride.setOnClickListener(this);
        advance_tiptoe.setOnClickListener(this);
        advance_straight.setOnClickListener(this);
        //高階篇動作畫面(選單中)
        highlevel_stand.setOnClickListener(this);
        highlevel_walk.setOnClickListener(this);
        //highlevel_squatdown.setOnClickListener(this);
        highlevel_ball.setOnClickListener(this);
        //三個難度(在各自的xml)
        el_advance.initExpand(false);
        el_elementary.initExpand(false);
        el_highlevel.initExpand(false);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //初階篇動作畫面(選單中)
            case R.id.elementary_tiptoe:
                flag_ele = 1;
                //按下按鈕後會轉至FASTActivity的畫面(activity_fast.xml)
                //因此你可以參照FASTActivity、activity_fast.xml做出其他畫面
                //總共 初階*2 + 進階*3 + 高階*4 = 9個activity與其xml
                Intent intent = new Intent(getApplicationContext(), BalancePicture1Activity.class);
                startActivity(intent);
                break;
            case R.id.elementary_stride:
                flag_ele = 2;
                intent = new Intent(getApplicationContext(), BalancePicture2Activity.class);
                startActivity(intent);
                break;
            //進階篇動作畫面(選單中)
            case R.id.advance_tiptoe:
                flag_adv = 1;
                intent = new Intent(getApplicationContext(), BalancePicture3Activity.class);
                startActivity(intent);
                break;
            case R.id.advance_stride:
                flag_adv = 2;
                intent = new Intent(getApplicationContext(), BalancePicture4Activity.class);
                startActivity(intent);
                break;
            case R.id.advance_straight:
                flag_adv = 3;
                intent = new Intent(getApplicationContext(), BalancePicture5Activity.class);
                startActivity(intent);
                break;
            //高階篇動作畫面(選單中)
            case R.id.highlevel_stand:
                flag_high = 1;
                intent = new Intent(getApplicationContext(), BalancePicture6Activity.class);
                startActivity(intent);
                break;
            case R.id.highlevel_walk:
                flag_high = 2;
                intent = new Intent(getApplicationContext(), BalancePicture7Activity.class);
                startActivity(intent);
                break;
            /*case R.id.highlevel_squatdown:
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                handler.sendMessageDelayed(handler.obtainMessage(1), 0);
                type = 9;
                Sudoku_mode = High_level_Mode;
                break;*/
            case R.id.highlevel_ball:
                flag_high = 3;
                intent = new Intent(getApplicationContext(), BalancePicture8Activity.class);
                startActivity(intent);
                break;
            case R.id.bal_menu:
                drawerLayout.openDrawer(bal_left_layout);//打开侧滑
                break;
            case R.id.bal_elementary:
                flag = 1;
                el_elementary.toggleExpand();
                el_advance.collapse();
                el_highlevel.collapse();
                break;
            case R.id.bal_advance:
                flag = 2;
                el_advance.toggleExpand();
                el_elementary.collapse();
                el_highlevel.collapse();
                break;
            case R.id.bal_highlevel:
                flag = 3;
                el_elementary.collapse();
                el_highlevel.toggleExpand();
                el_advance.collapse();
                break;

        }
        switch (flag){
            case 1:
                bal_elementary.setActivated(true);
                bal_advance.setActivated(false);
                bal_highlevel.setActivated(false);
                break;
            case 2:
                bal_elementary.setActivated(false);
                bal_advance.setActivated(true);
                bal_highlevel.setActivated(false);
                break;
            case 3:
                bal_elementary.setActivated(false);
                bal_advance.setActivated(false);
                bal_highlevel.setActivated(true);
                break;
        }
        switch (flag_ele){
            case 1:
                Log.d(TAG, "ele");
                elementary_tiptoe.setActivated(true);
                break;
            case 2:
                elementary_stride.setActivated(true);
                break;
        }
        switch (flag_adv){
            case 1:
                advance_tiptoe.setActivated(true);
                break;
            case 2:
                advance_stride.setActivated(true);
                break;
            case 3:
                advance_straight.setActivated(true);
                break;
        }
        switch (flag_high){
            case 1:
                highlevel_stand.setActivated(true);
                break;
            case 2:
                highlevel_walk.setActivated(true);
                break;
            case 3:
                highlevel_ball.setActivated(true);
                break;
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        //允许侧边滑动
        if (drawerLayout != null) {
            //bal_start.setText("點我開始");
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    //键盘返回键监听
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    //处理键盘事件
    private void exit() {
        if (!isExit) {
            isExit = true;
            Toast.makeText(BalanceActivity.this, "再按一次退出動作平衡訓練", Toast.LENGTH_SHORT).show();
            mHandler.sendEmptyMessageDelayed(0, 2000);
        } else {
            Intent intent = new Intent(getApplicationContext(), PreventActivity.class);
            startActivity(intent);
        }
    }

    @SuppressLint("HandlerLeak")
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            isExit = false;
        }
    };
}