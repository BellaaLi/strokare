package com.example.mytest.accelerometer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class Register extends Fragment implements View.OnClickListener{
    EditText mEdt_username;
    EditText mEdt_password;
    EditText mEdt_email;
    TextView mtext_email;
    TextView mtext_username;
    TextView mtext_password1;
    TextView mtext_password2;
    CheckBox mCheckB_password;
    Boolean mEdt_username_format_isok;
    Boolean mEdt_password_format_isok;
    Boolean mEdt_email_format_isok;
    Boolean edtIsEmpty;
    Button regBtn;
    Button logBtn;
    ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        //載入register介面佈局檔，得到一個View物件
        View rootView = inflater.inflate(R.layout.register, container, false);

        rootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                /**隱藏軟鍵盤**/

                InputMethodManager inputmanger = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputmanger.hideSoftInputFromWindow(mEdt_username.getWindowToken(), 0);
                return false;
            }
        });
        regBtn = (Button) rootView.findViewById(R.id.registration_button);
        logBtn = (Button) rootView.findViewById(R.id.to_login_button);

        regBtn.setEnabled(false);   //一開始註冊按鈕設為不能按，等到帳號密碼信箱都輸入完才變成可以按
        //regBtn.setPressed(true);
        //regBtn.setClickable(false);

        mEdt_username = (EditText) rootView.findViewById(R.id.reg_username);
        mEdt_password = (EditText) rootView.findViewById(R.id.reg_password);
        mtext_email = (TextView) rootView.findViewById(R.id.email_hint_text);
        mtext_username = (TextView) rootView.findViewById(R.id.username_hint_text);
        mtext_password1 = (TextView) rootView.findViewById(R.id.password_hint_text1);
        mtext_password2 = (TextView) rootView.findViewById(R.id.password_hint_text2);
        //隱藏密碼
        mEdt_password.setTransformationMethod(PasswordTransformationMethod.getInstance());
        mEdt_email = (EditText) rootView.findViewById(R.id.reg_email);
        mCheckB_password = (CheckBox)rootView.findViewById(R.id.password_checkBox); //要不要顯示密碼的選項

        //等待畫面
        progressBar = rootView.findViewById(R.id.indeterminateBar_register);

        //一開始email, password, username格式正確都設成false
        mEdt_email_format_isok = false;
        mEdt_username_format_isok = false;
        mEdt_password_format_isok = false;
        edtIsEmpty = true;


        regBtn.setOnClickListener(this); //點擊註冊按鈕產生事件
        logBtn.setOnClickListener(this); //點擊登入按鈕產生事件

        //透過勾選checkbox來顯示或隱藏密碼
        mCheckB_password.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // TODO Auto-generated method stub
                if(isChecked){
                    //如果選中，顯示密碼
                    mEdt_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }else{
                    //否則隱藏密碼
                    mEdt_password.setTransformationMethod(PasswordTransformationMethod.getInstance());

                }

            }
        });

        //username的editText加監聽事件，檢查輸入的字串長度和有沒有使用特殊符號
        mEdt_username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                isUsernameValid(mEdt_username.getText().toString());
                isEmptyEditTextLogin();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isUsernameValid(mEdt_username.getText().toString());
                isEmptyEditTextLogin();
                regBtnSetting();
            }

            @Override
            public void afterTextChanged(Editable s) {
                isUsernameValid(mEdt_username.getText().toString());
                isEmptyEditTextLogin();
                regBtnSetting();
            }
        });

        //password的editText加監聽事件，檢查輸入的字串長度和有沒有使用特殊符號
        mEdt_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                isPasswordValid(mEdt_password.getText().toString());
                //isPasswordValid(s);
                isEmptyEditTextLogin();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isPasswordValid(mEdt_password.getText().toString());
                //isPasswordValid(s);
                //Log.d("on_inputtext", mEdt_password.getText().toString());
                //if(mEdt_password_format_isok){Log.d("on_pswdok", "true");}
                //else {Log.d("on_pswdok", "false");}
                isEmptyEditTextLogin();
                regBtnSetting();
            }

            @Override
            public void afterTextChanged(Editable s) {
                isPasswordValid(mEdt_password.getText().toString());
                //Log.d("after_inputtext", mEdt_password.getText().toString());
                //isPasswordValid(s);
                isEmptyEditTextLogin();
                regBtnSetting();

            }
        });

        //email的editText加監聽事件，檢查輸入的信箱是否格式正確
        mEdt_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //檢查信箱格式
                //checkEmailValid(mEdt_email.getText().toString());
                isEmailValid(mEdt_email.getText().toString());
                isEmptyEditTextLogin();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                //檢查信箱格式
                //checkEmailValid(mEdt_email.getText().toString());
                isEmailValid(mEdt_email.getText().toString());
                isEmptyEditTextLogin();
                regBtnSetting();
            }

            @Override
            public void afterTextChanged(Editable s) {
                //檢查信箱格式
                //checkEmailValid(mEdt_email.getText().toString());
                isEmailValid(mEdt_email.getText().toString());
                isEmptyEditTextLogin();
                regBtnSetting();
            }

        });

        return rootView;




    }

    //帳號格式是否正確
    public boolean isUsernameValid(CharSequence username) {

        /*檢查有無特殊符號*/
        String limitEx="[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~!@#￥%……&*()——+|{}【】‘;:”“’。,、?\\u4e00-\\u9fa5]";

        Pattern pattern = Pattern.compile(limitEx);
        Matcher m = pattern.matcher(username);
        //含有特殊字元
        if( m.find()){
            mtext_username.setText("帳號含有特殊字元");
            mtext_username.setTextColor(Color.RED);
            mEdt_username_format_isok = false;


            return false;
        }

        mtext_username.setText("");
        mEdt_username_format_isok = true;


        return true;
    }

    //密碼格式是否正確
    public boolean isPasswordValid(CharSequence password) {
        /*檢查有無特殊符號*/
        String limitEx="[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~!@#￥%……&*()——+|{}【】‘;:”“’。,\\-、?_]";

        Pattern pattern = Pattern.compile(limitEx);
        //中文
        Pattern p_forchinese =Pattern.compile("[\\u4e00-\\u9fa5？；。、！–【】，：（）｛｝《》「」‘]");
        Matcher m = pattern.matcher(password);
        Matcher m_forchinese = p_forchinese.matcher(password);
        Boolean have_special_char = m.find() || m_forchinese.find();
        if(!have_special_char && editTextLength(mEdt_password)>=6 && editTextLength(mEdt_password)<=18){
            mtext_password1.setText("✓ 密碼長度6~18");
            mtext_password1.setTextColor(Color.parseColor("#00DD00"));
            mtext_password2.setText("✓ 密碼不含特殊字元");
            mtext_password2.setTextColor(Color.parseColor("#00DD00"));
            mEdt_password_format_isok = true;
            //帳號密碼信箱格式正確時，註冊按鈕設為可以按
            //regBtnSetting();
            return true;
        }
        else {

            //含有特殊字元
            if (have_special_char) {
                /*if(mtext_password1.getText().toString().equals("")) {
                    mtext_password1.setText("密碼含有特殊字元");
                    mtext_password1.setTextColor(Color.RED);
                }
                else {
                    mtext_password2.setText("密碼含有特殊字元");
                    mtext_password2.setTextColor(Color.RED);
                }*/
                mtext_password2.setText("X 密碼不含特殊字元");
                mtext_password2.setTextColor(Color.parseColor("#AAAAAA"));
                mEdt_password_format_isok = false;
            }else{
                mtext_password2.setText("✓ 密碼不含特殊字元");
                mtext_password2.setTextColor(Color.parseColor("#00DD00"));
            }

            /*檢查密碼長度(6~18)*/
            /*if (editTextLength(mEdt_password) < 6) {
                if(mtext_password1.getText().toString().equals("")) {
                    mtext_password1.setText("密碼長度不足");
                    mtext_password1.setTextColor(Color.RED);
                }
                else{
                    mtext_password2.setText("密碼長度不足");
                    mtext_password2.setTextColor(Color.RED);
                }
                mEdt_password_format_isok = false;

            }
            if (editTextLength(mEdt_password) > 18) {
                if(mtext_password1.getText().toString().equals("")) {
                    mtext_password1.setText("密碼長度太長");
                    mtext_password1.setTextColor(Color.RED);
                }
                else {
                    mtext_password2.setText("密碼長度太長");
                    mtext_password2.setTextColor(Color.RED);
                }
                mEdt_password_format_isok = false;
            }*/
            if (editTextLength(mEdt_password) < 6 || editTextLength(mEdt_password) > 18) {
                mtext_password1.setText("X 密碼長度6~18");
                mtext_password1.setTextColor(Color.parseColor("#AAAAAA"));
                mEdt_password_format_isok = false;
            }else{
                mtext_password1.setText("✓ 密碼長度6~18");
                mtext_password1.setTextColor(Color.parseColor("#00DD00"));
            }


            //regBtnSetting();
            return false;
        }


    }

    //信箱格式是否正確
    public boolean isEmailValid(CharSequence email) {
        //信箱格式正確
        if(android.util.Patterns.EMAIL_ADDRESS.matcher(email)
                .matches()){
            mtext_email.setText("");
            mEdt_email_format_isok = true;
            //帳號密碼信箱格式正確時，註冊按鈕設為可以按
            //regBtnSetting();
            return true;
        }
        //信箱格式錯誤
        else{
            mtext_email.setText("信箱格式不正確");
            mtext_email.setTextColor(Color.RED);
            mEdt_email_format_isok = false;
            //regBtnSetting();
            return false;
        }

    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.registration_button:
                regBtn.setEnabled(false);
                //regBtn.setPressed(true);
                //regBtn.setClickable(false);
                RegButtonClick();
                break;
            case R.id.to_login_button:
                LogButtonClick();
                break;
        }
    }


    //切換fragment
    public void replaceFragment(Fragment someFragment) {

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }





    //返回鍵退出
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        //go to previous fragemnt
                        //perform your fragment transaction here
                        //pass data as arguments
                        getActivity().finish();
                        return true;

                    }
                }
                return false;
            }
        });
    }





    //點擊註冊按鈕後會把使用者輸入的值給django，存到sqlite
    public void RegButtonClick()
    {


        String str_reg_username = mEdt_username.getText().toString();
        String str_reg_password = mEdt_password.getText().toString();
        String str_reg_email = mEdt_email.getText().toString();

        /**隱藏軟鍵盤**/

        InputMethodManager inputmanger = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputmanger.hideSoftInputFromWindow(mEdt_email.getWindowToken(), 0);


        //檢查使用者有沒有輸入帳號密碼和信箱
        if (!isEmptyEditTextLogin()){

            //帳號格式正確
            if(isUsernameValid(str_reg_username)) {
                //密碼格式正確
                if(isPasswordValid(str_reg_password)) {
                    //信箱格式正確
                    if (isEmailValid(str_reg_email)) {
                        //註冊到django sqlite
                        register(str_reg_username, str_reg_password, str_reg_email);
                        //等待畫面
                        show();
                    }
                    //信箱格式錯誤
                    else {
                        regBtn.setEnabled(false);
                        //regBtn.setPressed(true);
                        //regBtn.setClickable(false);
                        Toast.makeText(getContext(), "信箱格式不正確，請重新輸入", Toast.LENGTH_LONG).show();
                    }
                }
                //密碼格式錯誤
                else {
                    regBtn.setEnabled(false);
                    //regBtn.setPressed(true);
                    //regBtn.setClickable(false);
                    Toast.makeText(getContext(), "密碼格式不正確，請重新輸入", Toast.LENGTH_LONG).show();
                }
            }
            //帳號格式錯誤
            else {
                regBtn.setEnabled(false);
                //regBtn.setPressed(true);
                //regBtn.setClickable(false);
                Toast.makeText(getContext(), "帳號格式不正確，請重新輸入", Toast.LENGTH_LONG).show();
            }
        }
        //EditText有空的
        else{
            regBtn.setEnabled(false);
            //regBtn.setPressed(true);
            //regBtn.setClickable(false);
            Toast toast = Toast.makeText(getActivity(),"請輸入帳號密碼、信箱", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }


    }

    //到登入頁面
    public void LogButtonClick()
    {

        Fragment fragment = null;
        fragment = new Login();
        replaceFragment(fragment);


    }

    //新增使用者帳號密碼、信箱到django的sqlite
    public void register(String username, String password, String email) {

        OkHttpClient client = new OkHttpClient().newBuilder()
                //.connectTimeout(1000000, TimeUnit.SECONDS)   // 設置連線Timeout
                .connectTimeout(15, TimeUnit.SECONDS)   // 設置連線Timeout
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
                .build();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", username);
            jsonObject.put("password", password);
            jsonObject.put("email", email);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        //MediaType JSON = MediaType.parse("application/json;");
        // put your json here
        RequestBody formBody = RequestBody.create(JSON, jsonObject.toString());

        //設定request連到django的login端點、為post連線
        Request request = new Request.Builder()
                //.url("http://10.0.0.2:8000")
                //.url("http://192.168.86.195:8000/register")
                .url("https://strokarebackend.herokuapp.com/register")
                .post(formBody) // 使用post連線
                //.header("Authorization", "Token b76479a84e64bff59bcfe390178662357a0e443b")
                .build();

        // 建立Call
        Call call = client.newCall(request);


        // 執行Call連線到網址
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 連線成功
                String result = response.body().string();
                String name;
                //String userID;
                try {
                    JSONObject Jobject = new JSONObject(result);
                    name = Jobject.getString("username");

                    //帳號已註冊過django會回傳{"username": "registered"}
                    if (name.equals("registered")) {

                        //要讓toast和AlertDialog在main thread執行
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                AlertDialog.Builder altDlgBuilder = new AlertDialog.Builder(getActivity());
                                altDlgBuilder.setTitle("註冊失敗");
                                altDlgBuilder.setMessage("此帳號已註冊，請重新註冊或登入");
                                //設定AlertDialog對話盒的按鈕
                                altDlgBuilder.setPositiveButton("是",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                moveout();
                                                mEdt_username.setText("");
                                                mEdt_password.setText("");
                                                mEdt_email.setText("");
                                            }
                                        });
                                altDlgBuilder.setNeutralButton("取消",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                //留在原本畫面
                                                moveout();
                                            }
                                        });
                                altDlgBuilder.show();
                                Toast.makeText(getContext(), "此帳號已註冊，請重新輸入", Toast.LENGTH_LONG).show();
                            }
                        });

                        //帳號未註冊過(註冊成功)
                    } else if (name != null) {

                        SharedPreferences preferences = getActivity().getSharedPreferences("register", Context.MODE_PRIVATE);
                        SharedPreferences.Editor prefLoginEdit = preferences.edit();
                        prefLoginEdit.putBoolean("registration", true);

                        prefLoginEdit.commit();

                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {

                                Toast.makeText(getContext(), "註冊成功", Toast.LENGTH_SHORT).show();
                                //登入
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {

                                        //Toast.makeText(getActivity(), "error :(", Toast.LENGTH_SHORT).show();
                                        login(username,password);


                                    }
                                });
                                //login(username,password);
                                //從preference取user_id
                                //SharedPreferences preferences = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                                //String user_id = preferences.getString("user_id", "none");





                            }
                        });




                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {

                            Toast.makeText(getActivity(), "error :(", Toast.LENGTH_SHORT).show();
                            moveout();

                        }
                    });
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {
                // 連線失敗
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getActivity(), "連線失敗，請稍後再試", Toast.LENGTH_SHORT).show();
                        moveout();
                        try {
                            Thread.sleep(5000);
                            regBtn.setEnabled(true);
                            //regBtn.setPressed(false);
                            //regBtn.setClickable(true);
                        } catch (InterruptedException interruptedException) {
                            interruptedException.printStackTrace();
                        }
                    }
                });
            }
        });



    }


    //檢查帳號、密碼、信箱的EditText是否有輸入東西
    private Boolean isEmptyEditTextLogin(){


        if(mEdt_password.getText().toString().isEmpty() || mEdt_username.getText().toString().isEmpty()|| mEdt_email.getText().toString().isEmpty()){


            //regBtn.setEnabled(false);
            edtIsEmpty = true;


            return true;
        }else{
            //regBtn.setEnabled(true);
            edtIsEmpty = false;
            return false;
        }

    }

    //檢查edittext的字串長度
    private int editTextLength(EditText edt){
        return edt.length();
    }

    //帳號、密碼、信箱的editText格式正確才會讓註冊按鈕改成可以點擊
    private  void regBtnSetting(){
        //格式都正確則註冊按鈕設成可以點擊
        if (mEdt_username_format_isok && mEdt_password_format_isok && mEdt_email_format_isok && !edtIsEmpty){
            regBtn.setEnabled(true);
            //regBtn.setPressed(false);
            //regBtn.setClickable(true);
        }
        //格式不正確則註冊按鈕設成不能點擊
        else{
            regBtn.setEnabled(false);
            //regBtn.setPressed(true);
            //regBtn.setClickable(false);
        }
    }

    //註冊成功後登入取token和user_id
    private void login(String username,String password){
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(1000000, TimeUnit.SECONDS)   // 設置連線Timeout
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
                .build();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", username);
            jsonObject.put("password", password);
            //jsonObject.put("email", "newuser@gmail.com");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        // put your json here
        RequestBody formBody = RequestBody.create(JSON, jsonObject.toString());

        //設定request連到django的login端點、為post連線
        Request request = new Request.Builder()
                //.url("http://10.0.0.2:8000")
                //.url("http://192.168.86.195:8000/login")
                .url("https://strokarebackend.herokuapp.com/login")
                .post(formBody) // 使用post連線
                //.header("Authorization", "Token b76479a84e64bff59bcfe390178662357a0e443b")
                .build();

        // 建立Call
        Call call = client.newCall(request);
        // 執行Call連線到網址
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 連線成功
                String result = response.body().string();
                String token;
                String userID;
                try {
                    JSONObject Jobject = new JSONObject(result);

                    token = Jobject.getString("token");
                    userID = Jobject.getString("user_id");

                    SharedPreferences preferences = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                    SharedPreferences.Editor prefLoginEdit = preferences.edit();
                    prefLoginEdit.putBoolean("loggedin_state", true);
                    prefLoginEdit.putString("token", token);
                    prefLoginEdit.putString("user_id", userID);
                    prefLoginEdit.putString("username", username);
                    prefLoginEdit.commit();

                    //在django建這個user_id
                    newuserprofile(userID, mEdt_email.getText().toString());
                    //在django建立空的專注力訓練資料
                    newHistory(userID);


                } catch (JSONException e) {
                    e.printStackTrace();

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), "error :(", Toast.LENGTH_SHORT).show();
                            moveout();
                        }
                    });

                }

            }

            @Override
            public void onFailure(Call call, IOException e) {
                // 連線失敗
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getActivity(), "連線失敗，請稍後再試", Toast.LENGTH_SHORT).show();
                        moveout();
                    }
                });


            }
        });
    }

    //在django個人資料的資料庫建一筆新user的空資料
    private void newuserprofile(String user_id, String email){
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(1000000, TimeUnit.SECONDS)   // 設置連線Timeout
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
                .build();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", email);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        // put your json here
        RequestBody formBody = RequestBody.create(JSON, jsonObject.toString());

        //設定request連到django的login端點、為post連線
        Request request = new Request.Builder()
                //.url("http://10.0.0.2:8000")
                //.url("http://192.168.0.101:8000/login")
                //.url("http://192.168.218.195:8000/login")
                .url("https://strokarebackend.herokuapp.com/userdata/"+user_id+"/")
                .post(formBody) // 使用post連線
                //.header("Authorization", "Token b76479a84e64bff59bcfe390178662357a0e443b")
                .build();

        // 建立Call
        Call call = client.newCall(request);
        // 執行Call連線到網址
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 連線成功
                String result = response.body().string();

                try {
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            moveout();
                            AlertDialog.Builder altDlgBuilder = new AlertDialog.Builder(getActivity());
                            altDlgBuilder.setTitle("註冊成功");
                            altDlgBuilder.setMessage("前往填寫個人資料？");
                            //設定AlertDialog對話盒的按鈕
                            altDlgBuilder.setPositiveButton("是",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            //到個人資料
                                            Intent intent = new Intent();
                                            intent.setClass(getActivity(), UserProfile.class);
                                            startActivity(intent);


                                        }
                                    });
                            altDlgBuilder.setNeutralButton("之後填",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            //到主畫面
                                            Intent intent = new Intent();
                                            intent.setClass(getActivity(), HomeActivity.class);
                                            startActivity(intent);

                                        }
                                    });
                            altDlgBuilder.show();
                        }
                    });
                    JSONObject Jobject = new JSONObject(result);

                    String post_state = Jobject.getString("status");



                    if(post_state.equals("ok, This is built.")){
                        Log.d("build state","user profile built successfully!");
                    }
                    else if(post_state.equals("post is failed")){
                        Log.d("build state","user profile built fail!");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getActivity(), "error :(", Toast.LENGTH_SHORT).show();
                    Log.d("build state","user profile built fail!");
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            moveout();
                        }
                    });
                }

            }

            @Override
            public void onFailure(Call call, IOException e) {
                // 連線失敗
                /*
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getActivity(), "連線失敗，請稍後再試", Toast.LENGTH_SHORT).show();
                    }
                });

                 */
                moveout();
                Log.d("build state","user profile built fail!");



            }
        });
    }

    private void newHistory(String user_id){
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(1000000, TimeUnit.SECONDS)   // 設置連線Timeout
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
                .build();

        JSONObject jsonObject = new JSONObject();

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        // put your json here
        RequestBody formBody = RequestBody.create(JSON, jsonObject.toString());

        //get連線
        Request request = new Request.Builder()
                .url("https://strokarebackend.herokuapp.com/focushistory/"+user_id+"/")
                //.header("Authorization", "Token "+token)
                .post(formBody)
                .build();

        // 建立Call
        Call call = client.newCall(request);
        // 執行Call連線到網址
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 連線成功
                String result = response.body().string();

                try {
                    JSONObject Jobject = new JSONObject(result);
                    if(Jobject.getString("status").equals("failed")){
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(getActivity(), "error :(", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), "error :(", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }

            @Override
            public void onFailure(Call call, IOException e) {
                // 連線失敗
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getActivity(), "連線失敗，請稍後再試", Toast.LENGTH_SHORT).show();
                    }
                });





            }
        });
    }

    //顯示等待畫面
    private void show() {

        progressBar.setVisibility(View.VISIBLE);
        regBtn.setEnabled(false);
        //regBtn.setPressed(true);
        //regBtn.setClickable(false);
        logBtn.setEnabled(false);
        //logBtn.setPressed(true);
        //logBtn.setClickable(false);


    }

    //移除等待畫面
    private void moveout() {

        progressBar.setVisibility(View.GONE);
        regBtn.setEnabled(true);
        //regBtn.setPressed(false);
        //regBtn.setClickable(true);
        logBtn.setEnabled(true);
        //logBtn.setPressed(false);
        //logBtn.setClickable(true);


    }



}
