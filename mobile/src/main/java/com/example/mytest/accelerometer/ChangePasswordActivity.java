package com.example.mytest.accelerometer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener{

    EditText mEdt_oldpassword;
    EditText mEdt_newpassword;
    //Boolean mEdt_oldpassword_format_isok;
    Boolean mEdt_newpassword_format_isok;
    Boolean edtIsEmpty;
    Button changepassword_Btn;
    TextView mtext_username;
    TextView mtext_password1;
    TextView mtext_password2;
    String user_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        //取得帳號名稱
        SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        user_name = preferences.getString("username", "none");

        mtext_username = findViewById(R.id.cp_username);
        mtext_username.setText(user_name);
        mEdt_oldpassword = findViewById(R.id.cp_oldpassword);
        mEdt_newpassword = findViewById(R.id.cp_newpassword);
        mtext_password1 = findViewById(R.id.cp_password_hint_text1);
        mtext_password2 = findViewById(R.id.cp_password_hint_text2);

        changepassword_Btn = findViewById(R.id.changepassword_button);

        changepassword_Btn.setOnClickListener(this);
        changepassword_Btn.setEnabled(false);   //一開始讓修改按鈕設為不能按

        //隱藏密碼
        mEdt_oldpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        mEdt_newpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

        //輸入EditText時讓使用者點擊畫面會收起鍵盤

        changepassword_Btn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputmanger.hideSoftInputFromWindow(mtext_username.getWindowToken(), 0);
                return false;
            }
        });

        
        //一開始new password格式正確都設成false
        mEdt_newpassword_format_isok = false;
        edtIsEmpty = true;


        //oldpassword的editText加監聽事件，檢查輸入的字串長度和有沒有使用特殊符號
        mEdt_newpassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                isEmptyEditTextLogin();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isEmptyEditTextLogin();
                changeBtnSetting();
            }

            @Override
            public void afterTextChanged(Editable s) {
                isEmptyEditTextLogin();
                changeBtnSetting();

            }
        });

        //newpassword的editText加監聽事件，檢查輸入的字串長度和有沒有使用特殊符號
        mEdt_newpassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                isEmptyEditTextLogin();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isPasswordValid(mEdt_newpassword.getText().toString());
                isEmptyEditTextLogin();
                changeBtnSetting();
            }

            @Override
            public void afterTextChanged(Editable s) {
                isPasswordValid(mEdt_newpassword.getText().toString());
                isEmptyEditTextLogin();
                changeBtnSetting();

            }
        });


    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.changepassword_button:
                //修改密碼
                change_password();
                break;

        }
    }

    private void change_password(){

        //檢查原密碼有沒有輸入正確,正確則修改密碼，錯誤不修改
        checkpasswordexist();
        changepassword_Btn.setEnabled(false); //避免重複按按鈕

    }

    private void checkpasswordexist() {

        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(15, TimeUnit.SECONDS)   // 設置連線Timeout
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
                .build();


        //舊密碼
        String oldpswd = mEdt_oldpassword.getText().toString();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", user_name);
            jsonObject.put("password", oldpswd);
            Log.d("username", user_name);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        // put your json here
        RequestBody formBody = RequestBody.create(JSON, jsonObject.toString());

        //設定request連到django的login端點、為post連線
        Request request = new Request.Builder()
                .url("https://strokarebackend.herokuapp.com/login")
                .post(formBody) // 使用post連線
                .build();

        // 建立Call
        Call call = client.newCall(request);


        // 執行Call連線到網址
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 連線成功
                String result = response.body().string();
                String token;
                //String userID;
                try {
                    JSONObject Jobject = new JSONObject(result);
                    token = Jobject.getString("token");
                    //userID = Jobject.getString("user_id");
                    //密碼正確
                    if (!token.equals("unregistered") && !token.equals("passwordwrong")) {
                       //修改django密碼
                        puttodjango();

                    }
                    //密碼錯誤
                    else  {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(ChangePasswordActivity.this, "帳號或密碼錯誤,請重新輸入", Toast.LENGTH_LONG).show();
                                changepassword_Btn.setEnabled(true);
                            }
                        });

                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(ChangePasswordActivity.this, "error", Toast.LENGTH_SHORT).show();
                        }
                    });

                }

            }

            @Override
            public void onFailure(Call call, IOException e) {
                // 連線失敗
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(ChangePasswordActivity.this, "連線失敗，請稍後再試", Toast.LENGTH_SHORT).show();
                        try {
                            Thread.sleep(5000);
                            changepassword_Btn.setEnabled(true);
                        } catch (InterruptedException interruptedException) {
                            interruptedException.printStackTrace();
                        }

                    }
                });

            }
        });

    }

    private Boolean isEmptyEditTextLogin () {

        if(mEdt_oldpassword.getText().toString().isEmpty() || mEdt_newpassword.getText().toString().isEmpty()){

            edtIsEmpty = true;
            return true;
        }else{
            edtIsEmpty = false;
            return false;
        }

    }

    //新密碼格式是否正確
    public boolean isPasswordValid(CharSequence password) {
        /*特殊符號*/
        String limitEx="[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~!@#￥%……&*()——+|{}【】‘;:”“’。,\\-、?_]";
        //中文
        Pattern p_forchinese =Pattern.compile("[\\u4e00-\\u9fa5]");
        Pattern pattern = Pattern.compile(limitEx);
        /*檢查有無特殊符號*/
        Matcher m = pattern.matcher(password);
        /*檢查有無中文*/
        Matcher m_forchinese = p_forchinese.matcher(password);
        Boolean have_special_char = m.find() || m_forchinese.find();
        if(!have_special_char && editTextLength(mEdt_newpassword)>=6 && editTextLength(mEdt_newpassword)<=18){
            mtext_password1.setText("✓ 密碼長度6~18");
            mtext_password1.setTextColor(Color.parseColor("#00DD00"));
            mtext_password2.setText("✓ 密碼不含特殊字元");
            mtext_password2.setTextColor(Color.parseColor("#00DD00"));
            mEdt_newpassword_format_isok = true;
            Log.d("on_inputtext_ispassword", "ispassword_if");

            return true;
        }
        else {

            //含有特殊字元
            if (have_special_char) {

                mtext_password2.setText("X 密碼不含特殊字元");
                mtext_password2.setTextColor(Color.parseColor("#AAAAAA"));
                Log.d("on_inputtext_ispassword", "ispassword_else_if");
                mEdt_newpassword_format_isok = false;
            }else{
                mtext_password2.setText("✓ 密碼不含特殊字元");
                mtext_password2.setTextColor(Color.parseColor("#00DD00"));
                Log.d("on_inputtext_ispassword", "ispassword_else_else");
            }


            if (editTextLength(mEdt_newpassword) < 6 || editTextLength(mEdt_newpassword) > 18) {
                mtext_password1.setText("X 密碼長度6~18");
                mtext_password1.setTextColor(Color.parseColor("#AAAAAA"));
                mEdt_newpassword_format_isok = false;
            }else{
                mtext_password1.setText("✓ 密碼長度6~18");
                mtext_password1.setTextColor(Color.parseColor("#00DD00"));
            }

            return false;
        }


    }

    //檢查edittext的字串長度
    private int editTextLength(EditText edt){
        return edt.length();
    }

    //帳號、密碼的editText格式正確才會讓註冊按鈕改成可以點擊
    private  void changeBtnSetting(){
        //格式都正確則註冊按鈕設成可以點擊
        if (mEdt_newpassword_format_isok && !edtIsEmpty){
            changepassword_Btn.setEnabled(true);
        }
        //格式不正確則註冊按鈕設成不能點擊
        else{
            changepassword_Btn.setEnabled(false);
        }
    }

    private void puttodjango(){
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(15, TimeUnit.SECONDS)   // 設置連線Timeout
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
                .build();


        //舊密碼
        String newpswd = mEdt_newpassword.getText().toString();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", user_name);
            jsonObject.put("password", newpswd);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        // put your json here
        RequestBody formBody = RequestBody.create(JSON, jsonObject.toString());

        //設定request連到django的login端點、為post連線
        Request request = new Request.Builder()
                .url("https://strokarebackend.herokuapp.com/register")
                .put(formBody) // 使用put連線
                .build();

        // 建立Call
        Call call = client.newCall(request);


        // 執行Call連線到網址
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 連線成功
                String result = response.body().string();

                try {
                    JSONObject Jobject = new JSONObject(result);
                    String username = Jobject.getString("username");


                        runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(ChangePasswordActivity.this, "修改成功", Toast.LENGTH_LONG).show();
                                changepassword_Btn.setEnabled(true);
                            }
                        });


                } catch (JSONException e) {
                    e.printStackTrace();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(ChangePasswordActivity.this, "error", Toast.LENGTH_SHORT).show();
                            changepassword_Btn.setEnabled(true);
                        }
                    });

                }

            }

            @Override
            public void onFailure(Call call, IOException e) {
                // 連線失敗
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(ChangePasswordActivity.this, "連線失敗，請稍後再試", Toast.LENGTH_SHORT).show();
                        try {
                            Thread.sleep(5000);
                            changepassword_Btn.setEnabled(true);
                        } catch (InterruptedException interruptedException) {
                            interruptedException.printStackTrace();
                        }

                    }
                });

            }
        });

    }

}