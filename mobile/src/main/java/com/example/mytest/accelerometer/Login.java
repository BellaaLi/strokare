package com.example.mytest.accelerometer;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.DataType;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

import static android.app.Activity.RESULT_OK;

public class Login extends Fragment implements View.OnClickListener {
    private static final String TAG = "Login";

    EditText mEdt_username;
    EditText mEdt_password;
    CheckBox mCheckB_password;
    Boolean mEdt_username_format_isok;
    Boolean mEdt_password_format_isok;
    Boolean edtIsEmpty;
    Button logBtn;
    Button to_reg_Btn;
    TextView mtext_username;
    TextView mtext_password1;
    TextView mtext_password2;
    ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.login, container, false);
        //輸入EditText時讓使用者點擊畫面會收起鍵盤
        rootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                /**隱藏軟鍵盤**/

                InputMethodManager inputmanger = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputmanger.hideSoftInputFromWindow(mEdt_username.getWindowToken(), 0);
                return false;
            }
        });


        logBtn = (Button) rootView.findViewById(R.id.login_button);
        logBtn.setEnabled(false);   //一開始讓登入按鈕設為不能按
        //logBtn.setPressed(true);
        //logBtn.setClickable(false);
        to_reg_Btn = (Button) rootView.findViewById(R.id.to_registration_button);

        mEdt_username = (EditText) rootView.findViewById(R.id.login_username);
        mEdt_password = (EditText) rootView.findViewById(R.id.login_password);
        mtext_username = (TextView) rootView.findViewById(R.id.login_username_hint_text);
        mtext_password1 = (TextView) rootView.findViewById(R.id.login_password_hint_text1);
        mtext_password2 = (TextView) rootView.findViewById(R.id.login_password_hint_text2);
        //隱藏密碼
        mEdt_password.setTransformationMethod(PasswordTransformationMethod.getInstance());

        mCheckB_password = (CheckBox)rootView.findViewById(R.id.login_password_checkBox); //要不要顯示密碼的選項

        //等待畫面
        progressBar = rootView.findViewById(R.id.indeterminateBar_login);

        //一開始password, username格式正確都設成false
        mEdt_username_format_isok = false;
        mEdt_password_format_isok = false;
        edtIsEmpty = true;


        //按鈕加入監聽事件
        logBtn.setOnClickListener(this);
        to_reg_Btn.setOnClickListener(this);

        //透過勾選checkbox來顯示或隱藏密碼
        mCheckB_password.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // TODO Auto-generated method stub
                if(isChecked){
                    //如果選中，顯示密碼
                    mEdt_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }else{
                    //否則隱藏密碼
                    mEdt_password.setTransformationMethod(PasswordTransformationMethod.getInstance());

                }

            }
        });

        //username的editText加監聽事件，檢查輸入的字串長度和有沒有使用特殊符號
        mEdt_username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                isUsernameValid(mEdt_username.getText().toString());
                isEmptyEditTextLogin();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isUsernameValid(mEdt_username.getText().toString());
                isEmptyEditTextLogin();
                regBtnSetting();
            }

            @Override
            public void afterTextChanged(Editable s) {
                isUsernameValid(mEdt_username.getText().toString());
                isEmptyEditTextLogin();
                regBtnSetting();
            }
        });

        //password的editText加監聽事件，檢查輸入的字串長度和有沒有使用特殊符號
        mEdt_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //isPasswordValid(mEdt_password.getText().toString());
                //isPasswordValid(s);
                isEmptyEditTextLogin();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isPasswordValid(mEdt_password.getText().toString());
                //isPasswordValid(s);
                Log.d("on_inputtext", mEdt_password.getText().toString());
                if(mEdt_password_format_isok){Log.d("on_pswdok", "true");}
                else {Log.d("on_pswdok", "false");}
                isEmptyEditTextLogin();
                regBtnSetting();
            }

            @Override
            public void afterTextChanged(Editable s) {
                isPasswordValid(mEdt_password.getText().toString());
                Log.d("after_inputtext", mEdt_password.getText().toString());
                //isPasswordValid(s);
                isEmptyEditTextLogin();
                regBtnSetting();

            }
        });

        return rootView;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_button:
                logBtn.setEnabled(false);
                //logBtn.setPressed(true);
                //logBtn.setClickable(false);
                Log.i(TAG,"R.id.login_button");
                LogButtonClick();
                break;
            case R.id.to_registration_button:
                Log.i(TAG,"R.id.to_registration_button");
                RegButtonClick();
                break;
        }
    }


    public void replaceFragment(Fragment someFragment) {

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }


    public void RegButtonClick() {

        //前往註冊頁面
        Fragment fragment = null;
        fragment = new Register();
        replaceFragment(fragment);

    }


    public void LogButtonClick() {
        //username和password都有輸入值
        if (!isEmptyEditTextLogin()) {
            //判斷uername長度、有無特殊字元(是否為無效名字)
            //判斷password長度、有無特殊字元(是否為無效密碼)

            show();
            login();
        }
    }



    private void login() {

        OkHttpClient client = new OkHttpClient().newBuilder()
                //.connectTimeout(1000000, TimeUnit.SECONDS)   // 設置連線Timeout
                .connectTimeout(15, TimeUnit.SECONDS)   // 設置連線Timeout
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
                .build();


        String name = mEdt_username.getText().toString();
        String pswd = mEdt_password.getText().toString();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", name);
            jsonObject.put("password", pswd);
            //jsonObject.put("email", "newuser@gmail.com");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        //MediaType JSON = MediaType.parse("application/json;");
        // put your json here
        RequestBody formBody = RequestBody.create(JSON, jsonObject.toString());

        //設定request連到django的login端點、為post連線
        Request request = new Request.Builder()
                //.url("http://10.0.0.2:8000")
                //.url("http://192.168.0.101:8000/login")
                .url("https://strokarebackend.herokuapp.com/login")
                .post(formBody) // 使用post連線
                //.header("Authorization", "Token b76479a84e64bff59bcfe390178662357a0e443b")
                .build();

        // 建立Call
        Call call = client.newCall(request);


        // 執行Call連線到網址
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 連線成功
                String result = response.body().string();
                String token;
                String userID;
                try {
                    JSONObject Jobject = new JSONObject(result);
                    token = Jobject.getString("token");
                    userID = Jobject.getString("user_id");
                    if (token.equals("unregistered")) {

                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                moveout();
                                Toast.makeText(getContext(), "此帳號未註冊", Toast.LENGTH_LONG).show();
                                AlertDialog.Builder altDlgBuilder = new AlertDialog.Builder(getActivity());
                                altDlgBuilder.setTitle("登入失敗");
                                altDlgBuilder.setMessage("此帳號未註冊，前往註冊？");
                                //設定AlertDialog對話盒的按鈕
                                altDlgBuilder.setPositiveButton("是",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                //前往註冊頁面
                                                Fragment fragment = null;
                                                fragment = new Register();
                                                replaceFragment(fragment);
                                            }
                                        });
                                altDlgBuilder.setNeutralButton("取消",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                //留在原本畫面
                                            }
                                        });
                                altDlgBuilder.show();
                            }
                        });

                    }
                    else if (token.equals("passwordwrong")) {


                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                moveout();
                                Toast.makeText(getContext(), "帳號或密碼錯誤", Toast.LENGTH_LONG).show();
                                AlertDialog.Builder altDlgBuilder = new AlertDialog.Builder(getActivity());
                                altDlgBuilder.setTitle("登入失敗");
                                altDlgBuilder.setMessage("請重新輸入");
                                //設定AlertDialog對話盒的按鈕
                                altDlgBuilder.setPositiveButton("是",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                //留在原頁面
                                            }
                                        });

                                altDlgBuilder.show();
                            }
                        });

                    }
                    else if (token != null) {

                        SharedPreferences preferences = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                        SharedPreferences.Editor prefLoginEdit = preferences.edit();
                        prefLoginEdit.putBoolean("loggedin_state", true);
                        prefLoginEdit.putString("token", token);
                        prefLoginEdit.putString("user_id", userID);
                        prefLoginEdit.putString("username", name);
                        prefLoginEdit.commit();

                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(getContext(), "登入成功", Toast.LENGTH_SHORT).show();
                                moveout();
                            }
                        });
                        //前往首頁
                        Intent it = new Intent();
                        it.setClass(getActivity(), HomeActivity.class);
                        startActivity(it);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
                            moveout();
                        }
                    });

                }

            }

            @Override
            public void onFailure(Call call, IOException e) {
                // 連線失敗

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getActivity(), "連線失敗，請稍後再試", Toast.LENGTH_SHORT).show();
                        moveout();
                        try {
                            Thread.sleep(5000);
                            logBtn.setEnabled(true);
                            //logBtn.setPressed(false);
                            //logBtn.setClickable(true);
                        } catch (InterruptedException interruptedException) {
                            interruptedException.printStackTrace();
                        }

                    }
                });

            }
        });


    }
    private Boolean isEmptyEditTextLogin () {

        if(mEdt_password.getText().toString().isEmpty() || mEdt_username.getText().toString().isEmpty()){

            //regBtn.setEnabled(false);
            edtIsEmpty = true;


            return true;
        }else{
            //regBtn.setEnabled(true);
            edtIsEmpty = false;
            return false;
        }

    }

    //帳號格式是否正確
    public boolean isUsernameValid(CharSequence username) {

        /*檢查有無特殊符號、中文*/
        String limitEx="[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~!@#￥%……&*()——+|{}【】‘;:”“’。,、?\\u4e00-\\u9fa5]";

        Pattern pattern = Pattern.compile(limitEx);
        Matcher m = pattern.matcher(username);
        //含有特殊字元
        if( m.find()){
            mtext_username.setText("帳號含有特殊字元");
            mtext_username.setTextColor(Color.RED);
            mEdt_username_format_isok = false;


            return false;
        }

        mtext_username.setText("");
        mEdt_username_format_isok = true;


        return true;
    }

    //密碼格式是否正確
    public boolean isPasswordValid(CharSequence password) {
        /*特殊符號*/
        String limitEx="[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~!@#￥%……&*()——+|{}【】‘;:”“’。,\\-、?_]";
        //中文
        Pattern p_forchinese =Pattern.compile("[\\u4e00-\\u9fa5]");
        Pattern pattern = Pattern.compile(limitEx);
        /*檢查有無特殊符號*/
        Matcher m = pattern.matcher(password);
        /*檢查有無中文*/
        Matcher m_forchinese = p_forchinese.matcher(password);
        Boolean have_special_char = m.find() || m_forchinese.find();
        if(!have_special_char && editTextLength(mEdt_password)>=6 && editTextLength(mEdt_password)<=18){
            mtext_password1.setText("✓ 密碼長度6~18");
            mtext_password1.setTextColor(Color.parseColor("#00DD00"));
            mtext_password2.setText("✓ 密碼不含特殊字元");
            mtext_password2.setTextColor(Color.parseColor("#00DD00"));
            mEdt_password_format_isok = true;
            Log.d("on_inputtext_ispassword", "ispassword_if");
            //帳號密碼信箱格式正確時，註冊按鈕設為可以按
            //regBtnSetting();
            return true;
        }
        else {

            //含有特殊字元
            if (have_special_char) {

                mtext_password2.setText("X 密碼不含特殊字元");
                mtext_password2.setTextColor(Color.parseColor("#AAAAAA"));
                Log.d("on_inputtext_ispassword", "ispassword_else_if");
                mEdt_password_format_isok = false;
            }else{
                mtext_password2.setText("✓ 密碼不含特殊字元");
                mtext_password2.setTextColor(Color.parseColor("#00DD00"));
                Log.d("on_inputtext_ispassword", "ispassword_else_else");
            }


            if (editTextLength(mEdt_password) < 6 || editTextLength(mEdt_password) > 18) {
                mtext_password1.setText("X 密碼長度6~18");
                mtext_password1.setTextColor(Color.parseColor("#AAAAAA"));
                mEdt_password_format_isok = false;
            }else{
                mtext_password1.setText("✓ 密碼長度6~18");
                mtext_password1.setTextColor(Color.parseColor("#00DD00"));
            }


            //regBtnSetting();
            return false;
        }


    }

    //檢查edittext的字串長度
    private int editTextLength(EditText edt){
        return edt.length();
    }

    //帳號、密碼的editText格式正確才會讓註冊按鈕改成可以點擊
    private  void regBtnSetting(){
        //格式都正確則註冊按鈕設成可以點擊
        if (mEdt_username_format_isok && mEdt_password_format_isok && !edtIsEmpty){
            logBtn.setEnabled(true);
            //logBtn.setPressed(false);
            //logBtn.setClickable(true);
        }
        //格式不正確則註冊按鈕設成不能點擊
        else{
            logBtn.setEnabled(false);
            //logBtn.setPressed(true);
            //logBtn.setClickable(false);
        }
    }


    //顯示等待畫面
    private void show() {

        progressBar.setVisibility(View.VISIBLE);
        logBtn.setEnabled(false);
        //logBtn.setPressed(true);
        //logBtn.setClickable(false);
        to_reg_Btn.setEnabled(false);
        //to_reg_Btn.setPressed(true);
        //to_reg_Btn.setClickable(false);
    }

    //移除等待畫面
    private void moveout() {

        progressBar.setVisibility(View.GONE);
        logBtn.setEnabled(true);
        //logBtn.setPressed(false);
        //logBtn.setClickable(true);
        to_reg_Btn.setEnabled(true);
        //to_reg_Btn.setPressed(false);
        //to_reg_Btn.setClickable(true);

    }


}
