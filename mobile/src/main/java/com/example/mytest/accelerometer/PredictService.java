package com.example.mytest.accelerometer;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.google.android.gms.wearable.MessageClient;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;


public class PredictService extends Service implements MessageClient.OnMessageReceivedListener{
    private Handler mHandler;
    private Handler handler_1 = null;
    private HandlerThread handlerThread_1 = null;
    private String mConnectedNode;
    private static boolean isStart ;  //紀錄有無啟動
    Queue<Double> right_predictData_queue = new ConcurrentLinkedQueue<Double>();
    Queue<Double> right_temporary_queue = new ConcurrentLinkedQueue<Double>();
    Queue<Double> left_predictData_queue = new ConcurrentLinkedQueue<Double>();
    Queue<Double> left_temporary_queue = new ConcurrentLinkedQueue<Double>();
    Iterator it_right ;
    Iterator it_left ;
    String user_id;
    private static final String TAG = "PredictService";
    int has_send_message = 0; //0:還沒傳 1:已傳過簡訊
    private String stroke_user_name;
    private String parent_phone;


    public PredictService() {
        mHandler = new Handler(Looper.myLooper());
        //建一個worker(新增一個員工 給他一個名字)
        handlerThread_1 = new HandlerThread("streaming_thread");


    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate()");
        super.onCreate();
        SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = preferences.getString("user_id", "none");
        Log.d("user_id", user_id);
        GetUserData getProfile = new GetUserData();
        getProfile.execute();
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand()");

        if (intent.getStringExtra("stop_running") != null && intent.getStringExtra("stop_running").equals("STOP")) {
            isStart = false;

            if(handlerThread_1.quit()) {
                handlerThread_1.quit();
                Wearable.getMessageClient(this).removeListener(this);
                stopForeground(true);
                stopSelf();
            }
        }
        else {
            isStart = true;

            Wearable.getMessageClient(this).addListener(this);
            NotificationManager notificationManager = null;
            String notificationId = "channelId";
            String notificationName = "MyService";
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            //創建notificationchannrel
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                NotificationChannel channel = new NotificationChannel(notificationId, notificationName, NotificationManager.IMPORTANCE_HIGH);

                notificationManager.createNotificationChannel(channel);

            }
            // 在API 11之後建Notification的方式
            Notification.Builder builder = new Notification.Builder(this.getApplicationContext()); //獲取一個Notification builder
            Intent nfIntent = new Intent(this, SensorActivity.class);
            builder.setContentIntent(PendingIntent.getActivity(this, 0, nfIntent, 0)) // 设置PendingIntent
                    .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher)) // 設置下拉表中的圖示(大圖標)
                    .setContentTitle("Strokare")// 設置下拉標題
                    .setSmallIcon(R.mipmap.ic_launcher) // 設置狀態欄內小圖示
                    .setContentText("偵測睡眠中風") // 設置内容
                    .setWhen(System.currentTimeMillis());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                builder.setChannelId(notificationId);
            }

            Notification notification = builder.build(); // 獲取Notification
            notification.defaults = Notification.DEFAULT_SOUND; //设置为默认的声音
            // 参数一：唯一的通知標示；参数二：通知消息。
            startForeground(1, notification);// 開始前台服務
            //mHandler = new Handler(Looper.myLooper());
            //建一個worker(新增一個員工 給他一個名字)
            //handlerThread_1 = new HandlerThread("streaming_thread");
            //讓Worker待命，等待其工作 (開啟Thread)(讓他開始上班)

            handlerThread_1.start();
            //3秒後再開始取temporary_queue的data
            try {
                Log.d(TAG, "handlerThread_1 sleep");
                handlerThread_1.sleep(3000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //新增一個老闆 他是worker的老闆
            handler_1 = new Handler(handlerThread_1.getLooper());
            //給handlerthread工作，做完閒置handlerthread(老闆指派員工1號去做事(runnable_1))
            handler_1.post(runnable_1);
        }
        return START_NOT_STICKY;

    }

    @Override
    public void onMessageReceived(MessageEvent event) {
        //右手加速度
        if (event.getPath().equals("/accelerometer_right")) {
            byte[] data = event.getData();
            final String SensorMessage = new String(data);
            final String[] xyz = SensorMessage.split(",");
            try {
                Double ax = Double.parseDouble(xyz[1]);
                Double ay = Double.parseDouble(xyz[2]);
                Double az = Double.parseDouble(xyz[3]);
                Double m = Math.sqrt(ax*ax + ay*ay + az*az);
                right_temporary_queue.add(m);
                Log.d(TAG, "right motion value:"+Double.toString(m));
            }catch (Exception e){
                right_temporary_queue.add(0.0);
                Log.d(TAG, "right motion value:"+Double.toString(0.0));
                e.printStackTrace();
            }
        }
        //左手加速度
        else if (event.getPath().equals("/accelerometer_left")) {
            byte[] data = event.getData();
            final String SensorMessage = new String(data);
            final String[] xyz = SensorMessage.split(",");
            try {
                Double ax = Double.parseDouble(xyz[1]);
                Double ay = Double.parseDouble(xyz[2]);
                Double az = Double.parseDouble(xyz[3]);
                Double m = Math.sqrt(ax*ax + ay*ay + az*az);
                left_temporary_queue.add(m);
                Log.d(TAG, "left motion value:"+Double.toString(m));
            }catch (Exception e){
                left_temporary_queue.add(0.0);
                Log.d(TAG, "left motion value:"+Double.toString(0.0));
                e.printStackTrace();
            }
        }
        //右手角速率
        /*else if (event.getPath().equals("/gyroscope_right")) {
            byte[] data = event.getData();
            final String SensorMessage = new String(data);
            final String[] xyz = SensorMessage.split(",");
        }*/
        //左手角速率
        /*else if (event.getPath().equals("/gyroscope_left")) {
            byte[] data = event.getData();
            final String SensorMessage = new String(data);
            final String[] xyz = SensorMessage.split(",");
        }*/
    }

    private Runnable runnable_1 = new Runnable() {

        @Override
        public void run() {

            //temporary_queue poll回傳為null時(表示queue裡面是空的)，motion_streaming_data設為0(此情況發生在手環沒錄到加速度資料時)
            //右手
            try {
                Double right_motion_streaming_data = right_temporary_queue.poll();
                right_predictData_queue.add(right_motion_streaming_data);
                Log.d(TAG, "motion value get from right_temporary_queue:"+Double.toString(right_motion_streaming_data));
            }catch (Exception e){
                right_predictData_queue.add(0.0);
                Log.d(TAG, "motion value get from right_temporary_queue: 0");
                e.printStackTrace();
            }
            //左手
            try {
                Double left_motion_streaming_data = left_temporary_queue.poll();
                left_predictData_queue.add(left_motion_streaming_data);
                Log.d(TAG, "motion value get from left_temporary_queue:"+Double.toString(left_motion_streaming_data));
            }catch (Exception e){
                left_predictData_queue.add(0.0);
                Log.d(TAG, "motion value get from left_temporary_queue: 0");
                e.printStackTrace();
            }
            //當queue蒐集到120分鐘大小的資料時(7200秒)，會將這120分鐘的資料傳給django預測並回傳是否有中風，以及刪除queue中第一分鐘的資料
            if(right_predictData_queue.size()>=7200){
                //if(predictData_queue.size()>=7200){
                JSONObject right_jsonObject = new JSONObject();
                JSONObject left_jsonObject = new JSONObject();
                JSONObject jsonObject2 = new JSONObject();
                //右手queue
                it_right = right_predictData_queue.iterator();
                int r_second_count = 0;
                while (it_right.hasNext()) {
                    String iteratorValue = (String) it_right.next().toString();
                    Log.d("queue", iteratorValue);
                    try {
                        right_jsonObject.put(Integer.toString(r_second_count), iteratorValue);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    r_second_count++;
                }
                try {
                    jsonObject2.put("right",right_jsonObject);
                }catch (JSONException e){
                    e.printStackTrace();
                }
                //左手queue
                it_left = left_predictData_queue.iterator();
                int l_second_count = 0;
                while (it_left.hasNext()) {
                    String iteratorValue = (String) it_left.next().toString();
                    Log.d("queue", iteratorValue);
                    try {
                        left_jsonObject.put(Integer.toString(l_second_count), iteratorValue);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    l_second_count++;
                }
                try {
                    jsonObject2.put("left",left_jsonObject);
                }catch (JSONException e){
                    e.printStackTrace();
                }
                try {
                    jsonObject2.put("user_id",user_id);
                }catch (JSONException e){
                    e.printStackTrace();
                }

                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                //MediaType JSON = MediaType.parse("application/json;");
                // put your json here
                RequestBody formBody = RequestBody.create(JSON, jsonObject2.toString());
                deliverToDjango(formBody);

                //刪掉predictData_queue最前面1分鐘的資料
                int i = 0;
                while (i < 60) {
                    right_predictData_queue.poll();
                    left_predictData_queue.poll();
                    i++;
                }
            }

            Log.d("queue_size", "right_predictData_queue size:"+Integer.toString(right_predictData_queue.size()));
            Log.d("queue_size", "left_predictData_queue size:"+Integer.toString(left_predictData_queue.size()));

            //handler指定每隔1秒要做一次工作 (單位毫秒:1000等於1秒)
            handler_1.postDelayed(this, 1000);


        }
    };
    // 讓其他class可以取得isStart
    public static boolean getIsStart(){
        return isStart;
    }

    private void deliverToDjango(RequestBody formBody) {

        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(1000000, TimeUnit.SECONDS)   // 設置連線Timeout
                //.connectTimeout(15, TimeUnit.SECONDS)   // 設置連線Timeout
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
                .build();



        //設定request連到django的login端點、為post連線
        Request request = new Request.Builder()
                //.url("http://10.0.0.2:8000")
                //.url("http://192.168.0.101:8000/deliver")
                .url("https://strokarebackend.herokuapp.com/deliver")
                .post(formBody) // 使用post連線
                //.header("Authorization", "Token b76479a84e64bff59bcfe390178662357a0e443b")
                .build();

        // 建立Call
        Call call = client.newCall(request);


        // 執行Call連線到網址
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 連線成功
                String result = response.body().string();
                String django_response;
                try {
                    JSONObject Jobject = new JSONObject(result);
                    django_response = Jobject.getString("result");


                    //runOnUiThread(new Runnable() {
                    //    public void run() {
                    //Toast.makeText(MyService.this, "成功", Toast.LENGTH_LONG).show();

                    //}
                    //});
                    /*new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public void run() {
                            Log.d("TAG","success");
                            //(測試用)看是否連線成功
                            Toast.makeText(getApplicationContext(), django_response, Toast.LENGTH_LONG).show();
                        }
                    });*/
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public void run() {
                            Log.d("TAG","success");
                            Toast toast = Toast.makeText(getApplicationContext(), django_response, Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER,0,0);
                            toast.show();
                        }
                    });

                    Bundle message = new Bundle();

                    if(django_response.equals("中風")){
                        message.putInt("predict", 1);
                        Intent intent = new Intent("ShowPredictResult");
                        intent.putExtras(message);
                        sendBroadcast(intent);
                        //還沒傳過簡訊
                        if(has_send_message == 0) {
                            //傳簡訊
                            sendSMSMessage();
                            has_send_message = 1;

                        }

                    }
                    if(django_response.equals("正常")){
                        message.putInt("predict", 0);
                        Intent intent = new Intent("ShowPredictResult");
                        intent.putExtras(message);
                        sendBroadcast(intent);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    //runOnUiThread(new Runnable() {
                    //    public void run() {
                    //Toast.makeText(MyService.this, "error", Toast.LENGTH_SHORT).show();

                    //    }
                    //});
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public void run() {
                            Log.d("TAG","error");
                            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
                        }
                    });


                }



            }

            @Override
            public void onFailure(Call call, IOException e) {
                // 連線失敗

                //runOnUiThread(new Runnable() {
                //    public void run() {
                //Log.d("TAG","fail");
                //Toast.makeText(MyService.this, "連線失敗，請稍後再試", Toast.LENGTH_SHORT).show();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        Log.d("TAG","fail");
                        Toast.makeText(getApplicationContext(), "連線失敗，請稍後再試", Toast.LENGTH_SHORT).show();
                    }
                });

                //}
                //});

            }
        });



    }

    protected void sendSMSMessage() {
        Log.i("Send SMS", "");


        try {
            String nowDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(parent_phone, null, "【Strokare】中風健康管理提醒您，您的家屬："+stroke_user_name+"於"+nowDate+
                    "偵測到疑似中風徵兆，請您快確認家屬狀況!",
                    null, null);
            Log.d("PredictService", "sms send");
        } catch (Exception e) {

            Log.d("PredictService", "SMS faild, please try again.");
            e.printStackTrace();
        }
    }

    //連線django GET 取得user_name
    private class GetUserData extends AsyncTask<Void, Void, String> {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson = "";


        //private String site_url = "http://192.168.86.195:8000/userdata/" + user_id + "/";
        private String site_url = "https://strokarebackend.herokuapp.com/userdata/" + user_id + "/";

        @Override
        protected String doInBackground(Void... params) {
            try {

                URL url = new URL(site_url);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();
                Log.i(TAG, "Connect!");

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                resultJson = buffer.toString();
                Log.i("Data", resultJson);


            } catch (Exception e) {
                //postProfile();
                //PostUserData myAsyncTasks = new PostUserData();
                //myAsyncTasks.execute();
                e.printStackTrace();
                Log.e(TAG, Log.getStackTraceString(e));
            }
            return resultJson;
        }


        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);
            try {
                strJson = strJson.replace("'", "\"");
                JSONObject jsonobj = new JSONObject(strJson);


                stroke_user_name = jsonobj.getString("user_name");
                parent_phone = jsonobj.getString("contactor_phone");

            } catch (JSONException e) {

                e.printStackTrace();
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }
    }


    //連到哪個NODE
    /*class getNodes_Thread extends Thread {
        getNodes_Thread() {
        }

        public void run() {
            //Retrieve the connected devices, known as nodes//
            Task<List<Node>> wearableList =
                    Wearable.getNodeClient(getApplicationContext()).getConnectedNodes();
            try {

                List<Node> nodes = Tasks.await(wearableList);
                for (Node node : nodes) {
                    mConnectedNode = node.getId();
                    Log.d(TAG, "The connected node is " + mConnectedNode);
                }
            } catch (ExecutionException exception) {
                Log.e(TAG, "Task failed: " + exception);
            } catch (InterruptedException exception) {
                Log.e(TAG, "Interrupt occurred: " + exception);
            }

        }
    }*/

}
