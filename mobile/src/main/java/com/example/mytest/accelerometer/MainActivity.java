package com.example.mytest.accelerometer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;


public class MainActivity extends AppCompatActivity{

    private static final String TAG = "MainActivity" ;
    private static final int GOOGLE_FIT_PERMISSIONS_REQUEST_CODE = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate");


        // 獲取手機權限
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACTIVITY_RECOGNITION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACTIVITY_RECOGNITION}, 101);
        }



        if (savedInstanceState == null) {
            SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);

            String token = preferences.getString("token", "none");
            String user_id = preferences.getString("user_id", "none");
            String user_name = preferences.getString("username", "none");
            Log.d(TAG, token);
            Log.d(TAG, user_id);
            Log.d(TAG, user_name);
            if (token != "none") {
                Intent intent = new Intent();
                //到主畫面
                intent.setClass(this, HomeActivity.class);
                startActivity(intent);
            } else {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new InitialFragment()).commit();
                //到登入註冊畫面
                /*Intent intent = new Intent();
                intent.setClass(this, InitialActivity.class);
                startActivity(intent);*/
            }
            /*getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new Login()).commit();*/
        }

    }


    /*@Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent();
        intent.setClass(this, HomeActivity.class);
        startActivity(intent);
    }*/
}