package com.example.mytest.accelerometer;

import android.app.DatePickerDialog;
import android.content.Context;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.util.ArrayUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserProfile extends AppCompatActivity {

    private EditText account, user_name, contactor_name, contactor_phone,
            height, weight;
    private Button update;
    private Button birth;
    private String data;
    private String TAG = "result";
    private Spinner sexual;
    private String user_id;
    final String[] sex = new String[1];
    final int[] pos = new int[1];
    final String[] lunch = {"請選擇","男", "女", "其他"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        //取得user_id
        SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = preferences.getString("user_id", "none");
//        user_id = Integer.toString(47);
        Log.d("user_id", user_id);

        update = findViewById(R.id.update);
        account = findViewById(R.id.account);
        user_name = findViewById(R.id.user_name);

        contactor_name = findViewById(R.id.contactor_name);
        contactor_phone = findViewById(R.id.phone);
        contactor_phone.setInputType(InputType.TYPE_CLASS_PHONE);

        height = findViewById(R.id.height);
        weight = findViewById(R.id.weight);


        sexual = findViewById(R.id.sex);
        ArrayAdapter<String> lunchList = new ArrayAdapter<>(UserProfile.this,
                android.R.layout.simple_spinner_dropdown_item, lunch);
        sexual.setAdapter(lunchList);

        sexual.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView parent, View view, int position, long id) {
                sex[0] = parent.getItemAtPosition(position).toString();
                pos[0] = position;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//
//        reset = findViewById(R.id.reset);
//        reset.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                user_name.setText("");
//                contactor_name.setText("");
//                contactor_phone.setText("");
//                height.setText("");
//                weight.setText("");
//            }
//        });

        birth = findViewById(R.id.birth);
        birth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                new DatePickerDialog(UserProfile.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        String format = setDateFormat(year, month, day);
                        birth.setText(format);
                    }

                }, mYear, mMonth, mDay).show();
            }

        });

        GetUserData getProfile = new GetUserData();
        getProfile.execute();

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(updateProfile()){
                    PutUserData myAsyncTasks = new PutUserData();
                    myAsyncTasks.execute();
                }
                else{
                    Context context = getApplicationContext();
                    CharSequence text = "資料有缺,請重新確認";
                    int duration = Toast.LENGTH_LONG;

                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast toast = Toast.makeText(context, text, duration);
                            toast.setGravity(Gravity.CENTER,0,0);
                            toast.show();
                        }
                    });
                }
            }
        });

    }

    private String setDateFormat(int year, int monthOfYear, int dayOfMonth) {
        return String.valueOf(year) + "-"
                + String.valueOf(monthOfYear + 1) + "-"
                + String.valueOf(dayOfMonth);
    }

    public boolean updateProfile() {

        Boolean complete = true;
        String email = account.getText().toString();
        String u_name = user_name.getText().toString();
        String birthday = birth.getText().toString();
        String c_name = contactor_name.getText().toString();
        String c_phone = contactor_phone.getText().toString();
        String h = height.getText().toString();
        String w = weight.getText().toString();
        //String sex = sexual.getSelectedItem().toString();



        if (u_name.isEmpty()) {
            user_name.setError("Your name is required !");
            user_name.requestFocus();
            complete = false;
        }

        if (birthday.isEmpty()) {
            birth.setError("Birthday is empty");
            birth.requestFocus();
            complete = false;
        }
        if (c_name.isEmpty()) {
            contactor_name.setError("Who is your contactor ?");
            contactor_name.requestFocus();
            complete = false;
        }
        if (c_phone.isEmpty()) {
            contactor_phone.setError("What is your contactor's phone ?");
            contactor_phone.requestFocus();
            complete = false;
        } else {
            Pattern pattern = Pattern.compile("(09)+[\\d]{8}");
            Matcher matcher = pattern.matcher(c_phone);
            int phone_lengh = c_phone.length();

            if (!matcher.find() || phone_lengh != 10) {
                contactor_phone.setError("Error phone number");
                contactor_phone.requestFocus();
                complete = false;
            }

        }
        if (h.isEmpty()) {
            height.setError("Please enter your height.");
            height.requestFocus();
            complete = false;
        }
        if (w.isEmpty()) {
            weight.setError("Please enter your weight.");
            weight.requestFocus();
            complete = false;
        }
        if(pos[0] ==0){
            TextView errorText = (TextView)sexual.getSelectedView();
            errorText.setTextColor(Color.RED);//just to highlight that this is an error
            errorText.setText("請選擇");//changes the selected item text to this
            complete = false;
        }

        if(complete)  {
            //把資料包成JSON
            ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
            HashMap<String, String> hashMap = new HashMap<>();//新增一個HashMap

            hashMap.put("account", email);
            hashMap.put("user_name", u_name);
            hashMap.put("birth_date", birthday);
            hashMap.put("sexual", lunch[pos[0]]);
            hashMap.put("contactor", c_name);
            hashMap.put("contactor_phone", c_phone);
            hashMap.put("height", h);
            hashMap.put("weight", w);

            arrayList.add(hashMap);

            data = new Gson().toJson(arrayList);//將此ArrayList轉為json字串
            data = data.replace("[", "");
            data = data.replace("]", "");
//            showjson.setText(data);
        }

        return complete;
    }


    /*public void postProfile() {

        //把資料包成JSON
        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
        HashMap<String, String> hashMap = new HashMap<>();//新增一個HashMap

        hashMap.put("email", "");
        arrayList.add(hashMap);

        data = new Gson().toJson(arrayList);//將此ArrayList轉為json字串
        data = data.replace("[", "");
        data = data.replace("]", "");


    }*/

    //連線django GET
    private class GetUserData extends AsyncTask<Void, Void, String> {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson = "";
//        String user_id = "47";

        //private String site_url = "http://192.168.86.195:8000/userdata/" + user_id + "/";
        private String site_url = "https://strokarebackend.herokuapp.com/userdata/" + user_id + "/";



        @Override
        protected String doInBackground(Void... params) {
            try {

                URL url = new URL(site_url);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();
                Log.i(TAG, "Connect!");

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                resultJson = buffer.toString();
                Log.i("Data", resultJson);


            } catch (Exception e) {
                //postProfile();
                //PostUserData myAsyncTasks = new PostUserData();
                //myAsyncTasks.execute();
                Context context = getApplicationContext();
                CharSequence text = "連線錯誤,請稍後再試!";
                int duration = Toast.LENGTH_LONG;


                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.setGravity(Gravity.CENTER,0,0);
                        toast.show();
                    }
                });

                e.printStackTrace();
                Log.e(TAG, Log.getStackTraceString(e));
            }
            return resultJson;
        }

        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);
            try {
                strJson = strJson.replace("'", "\"");
                JSONObject jsonobj = new JSONObject(strJson);

                //將所有結果透過setText由文字框印出
                account.setText(jsonobj.getString("account"));

                String name =  jsonobj.getString("user_name");
                if(name.equals("NAN")){ //還沒輸入姓名時
                    user_name.setText("");
                }else {
                    user_name.setText(name);
                }

                String bdate = jsonobj.getString("birth_date");
                if(bdate.equals("NAN")){ //還沒輸入生日時
                    birth.setText("");
                }else {
                    birth.setText(bdate);
                }

                String cname = jsonobj.getString("contactor");
                if(cname.equals("NAN")){ //還沒輸入聯絡人姓名時
                    contactor_name.setText("");
                }else {
                    contactor_name.setText(cname);
                }

                String cphone = jsonobj.getString("contactor_phone");
                if(cphone.equals("9999999999")){ //還沒輸入聯絡人電話時
                    contactor_phone.setText("");
                }else {
                    contactor_phone.setText(cphone);
                }

                String h = jsonobj.getString("height");
                if(h.equals("NAN")){ //還沒輸入身高時
                    height.setText("");
                }else {
                    height.setText(h);
                }

                String w = jsonobj.getString("weight");
                if(w.equals("NAN")){ //還沒輸入體重時
                    weight.setText("");
                }else {
                    weight.setText(w);
                }

                String sex_pos= jsonobj.getString("sexual");
                switch(sex_pos){
                    case "NAN":
                        sexual.setSelection(0,false);
                        break;
                    case "男":
                        sexual.setSelection(1,false);
                        break;
                    case "女":
                        sexual.setSelection(2,false);
                        break;
                    default:
                        sexual.setSelection(3,false);
                        break;
                }

            } catch (JSONException e) {

                e.printStackTrace();
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }

    }

    //PUT

    private class PutUserData extends AsyncTask<Void, Void, String> {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson = "";

        //String user_id = "5";
        //private String site_url = "http://192.168.86.195:8000/userdata/" + user_id + "/";
        private String site_url = "https://strokarebackend.herokuapp.com/userdata/" + user_id + "/";


        byte[] test = data.getBytes();

        @Override
        protected String doInBackground(Void... params) {

            try {

                URL url = new URL(site_url);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("PUT");
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setRequestProperty("Content-Type", "application/json; utf-8");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setFixedLengthStreamingMode(test.length);

                urlConnection.connect();


                Log.i(TAG, "Connect!");

                OutputStream os;
                os = urlConnection.getOutputStream();
                os.write(test);
                os.flush();
                os.close();


                InputStream inputStream = urlConnection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(inputStream));

                StringBuilder sb = new StringBuilder();
                String line;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                resultJson = sb.toString();
                Log.i(TAG, resultJson);
                Log.i(TAG, "Put done");

                Context context = getApplicationContext();
                CharSequence text = "更新資料成功!";
                int duration = Toast.LENGTH_LONG;

                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.setGravity(Gravity.CENTER,0,0);
                        toast.show();
                    }
                });




            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, Log.getStackTraceString(e));
                Log.i("Data", data);


                Context context = getApplicationContext();
                CharSequence text = "連線錯誤,請稍後再試!";
                int duration = Toast.LENGTH_LONG;


                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.setGravity(Gravity.CENTER,0,0);
                        toast.show();
                    }
                });


            }

            return data;
        }

    }

    //POST
/*
    private class PostUserData extends AsyncTask<Void, Void, String> {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson = "";

        //String user_id = "5";
        //private String site_url = "http://192.168.86.195:8000/userdata/" + user_id + "/";
        private String site_url = "https://strokarebackend.herokuapp.com/userdata/" + user_id + "/";


        byte[] test = data.getBytes();

        @Override
        protected String doInBackground(Void... params) {
            try {

                URL url = new URL(site_url);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setRequestProperty("Content-Type", "application/json; utf-8");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setFixedLengthStreamingMode(test.length);

                urlConnection.connect();


                Log.i(TAG, "Connect!");

                OutputStream os;
                os = urlConnection.getOutputStream();
                os.write(test);
                os.flush();
                os.close();


                InputStream inputStream = urlConnection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(inputStream));

                StringBuilder sb = new StringBuilder();
                String line;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                resultJson = sb.toString();
                Log.i(TAG, resultJson);
                Log.i(TAG, "Put done");

            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, Log.getStackTraceString(e));
                Log.i("Data", data);
            }

            return data;
        }

    }

 */
}
