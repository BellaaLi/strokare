package com.example.mytest.accelerometer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.card.MaterialCardView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class PreventActivity extends AppCompatActivity {

    private static final String TAG = "PreventActivity";
    MaterialCardView stroke_fast;
    MaterialCardView schulte_table;
    MaterialCardView action_balance;
    private TextView prevent_user_name;
    String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prevent);

        stroke_fast = (MaterialCardView) findViewById(R.id.fast_btn);
        schulte_table = (MaterialCardView) findViewById(R.id.focus_btn);
        action_balance = (MaterialCardView) findViewById(R.id.balance_btn);
        setTitle("");
        //取得user_id
        SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = preferences.getString("user_id", "none");
        Log.d(TAG,"user_id："+ user_id);
        prevent_user_name = findViewById(R.id.prevent_user_name);
        GetUserData getProfile = new GetUserData();
        getProfile.execute();


        stroke_fast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), FastActivity.class);
                startActivity(intent);
            }
        });

        schulte_table.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SelectionModeActivity.class);
                startActivity(intent);
            }
        });

        action_balance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), BalanceActivity.class);
                startActivity(intent);

            }
        });
    }
    //連線django GET 取得user_name
    private class GetUserData extends AsyncTask<Void, Void, String> {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson = "";
        //String user_id = "5";

        //private String site_url = "http://192.168.86.195:8000/userdata/" + user_id + "/";
        private String site_url = "https://strokarebackend.herokuapp.com/userdata/" + user_id + "/";

        @Override
        protected String doInBackground(Void... params) {
            try {

                URL url = new URL(site_url);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();
                Log.i(TAG, "Connect!");

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                resultJson = buffer.toString();
                Log.i("Data", resultJson);


            } catch (Exception e) {
                //postProfile();
                //PostUserData myAsyncTasks = new PostUserData();
                //myAsyncTasks.execute();
                e.printStackTrace();
                Log.e(TAG, Log.getStackTraceString(e));
            }
            return resultJson;
        }

        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);
            try {
                strJson = strJson.replace("'", "\"");
                JSONObject jsonobj = new JSONObject(strJson);

                //將所有結果透過setText由文字框印出
                String name =  jsonobj.getString("user_name");
                if(name.equals("NAN")){ //還沒輸入姓名時
                    Log.d(TAG, "onPostExecute: no name QQ");
                    prevent_user_name.setText("");
                }else {
                    prevent_user_name.setText(name+"！");
                }
            } catch (JSONException e) {

                e.printStackTrace();
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }

    }

}