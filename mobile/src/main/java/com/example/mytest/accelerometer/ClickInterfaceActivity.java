package com.example.mytest.accelerometer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mytest.accelerometer.adapter.MyAdapter;
import com.example.mytest.accelerometer.util.TableBorderLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class ClickInterfaceActivity extends AppCompatActivity {
    private SharedPreferences prefData_type;
    private static final String TAG = "time: ";
    private int type;
    private TextView timetime, countcount;
    private TableBorderLayout tableBorderLayout;
    private List<Integer> clickNumberList = new ArrayList<>();//需要点击的顺序
    private List<Integer> numberList = new ArrayList<>();//显示的数据，需要打乱的
    private int maxNumber = 0;//为最大值
    private int[] prime_number = new int[]{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
    private MyAdapter myAdapter;
    private int mlCount;
    private int sec;
    private String time;
    private Timer timer = null;
    private TimerTask task = null;
    private Message msg = null;
    private SharedPreferences.Editor editor;
    private SharedPreferences prefData_time;
    private String Sudoku_mode;
    private int clickNumber = 0;
    private boolean whetherColor = false;
    private List<Integer> isR = new ArrayList<>();
    private List<Integer> isG = new ArrayList<>();
    private List<Integer> isB = new ArrayList<>();

    private String historyTime;
    String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        prefData_type = getSharedPreferences("data-type", MODE_PRIVATE);//类型和个数
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_click_interface);
        //type 主要作用每行的个数， 以及方格的总个数
        type = getIntent().getIntExtra("tv_mode", -1);
        Sudoku_mode = getIntent().getStringExtra("Sudoku_mode");
        whetherColor = getIntent().getBooleanExtra("whetherColor", false);
        timetime = findViewById(R.id.timetime);
        countcount = findViewById(R.id.countcount);
        //实例化和获取 本地的 data-time time表示最好的成绩
        editor = getSharedPreferences("history_data", MODE_PRIVATE).edit();
        prefData_time = getSharedPreferences("history_data", MODE_PRIVATE);

        //取得user_id
        SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = preferences.getString("user_id", "none");


        //初始化view
        initViews();

        if (whetherColor == true) {
            setColor();
        }

        //设置数据
        setDatas();
        //设置adapter
        LayoutSetAdapter();
        startTime();


    }

    //设置颜色数据
    private void setColor() {
        isR.clear();
        isG.clear();
        isB.clear();
        for (int i = 130; i <= 255; i++) {
            isR.add(i);
            isG.add(i);
            isB.add(i);
        }
        //打乱数字
        Collections.shuffle(isR);
        Collections.shuffle(isG);
        Collections.shuffle(isB);
    }

    //结束时间
    private void stopTime() {
        mlCount = 0;
        sec = 0;
        task.cancel();
        timer.cancel();
        timer.purge();
        handler.removeMessages(msg.what);
    }

    //开始时间
    private void startTime() {
        task = new TimerTask() {
            @Override
            public void run() {
                if (msg == null) {
                    msg = new Message();
                } else {
                    msg = Message.obtain();
                }
                msg.what = 1;
                handler.sendMessage(msg);
            }
        };
        timer = new Timer();
        timer.schedule(task, 0, 1);
    }

    //初始化view
    private void initViews() {
        tableBorderLayout = findViewById(R.id.layTable);
    }

    //设置adapter
    private void LayoutSetAdapter() {
        if (whetherColor == true) {
            myAdapter = new MyAdapter(this, numberList, clickItem, type, isR, isG, isB);
            tableBorderLayout.setAdapter(myAdapter, type);
            Log.d(TAG, "顏色!!!");
        } else {
            myAdapter = new MyAdapter(this, numberList, clickItem, type);
            tableBorderLayout.setAdapter(myAdapter, type);
            Log.d(TAG, "白色!!!");
        }
    }

    //设置数据
    private void setDatas() {
        numberList.clear();
        clickNumberList.clear();
        if (Sudoku_mode.equals("normalMode")) {//普通模式：只用计算数量
            //初始化格数
            maxNumber = type * type;
            //遍历所有的数字，添加进集合
            for (int i = 1; i <= maxNumber; i++) {
                numberList.add(i);
                clickNumberList.add(i);
                countcount.setText("1");
            }
        } else if (Sudoku_mode.equals("PrimeNumberMode")) {
            maxNumber = prime_number.length;
            //遍历所有的数字，添加进集合
            for (int i = 0; i < prime_number.length; i++) {
                numberList.add(prime_number[i]);
                clickNumberList.add(prime_number[i]);
                countcount.setText("2");
            }
        } else if (Sudoku_mode.equals("OddNumberMode")) {
            maxNumber = type * type;
            for (int i = 1; i < 100; i += 2) {
                if (numberList.size() == maxNumber) break;
                numberList.add(i);
                clickNumberList.add(i);
                countcount.setText("1");
            }
        }

        //打乱数字
        Collections.shuffle(numberList);
    }

    //处理数字的点击事件
    public View.OnClickListener clickItem = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if ((clickNumber + 1) == maxNumber) {
                //1、结束之后需要和本地数据对比，小于本地的就是新纪录
                //2、当点击非正确数值时，要有提醒正确数值
                //String dialogMessage = "";//需要显示的提示
                String dataTime = "";//本次点击的时间
                //String historyTime = "";//历史点击的时间
                clickNumber = 0;
                /*
                time.compareTo(historyTime)

                1、字符串相等返回0
                2、前字符串 大于后 字符串 返回正值
                3、前字符串 小于 后字符串 返回负值
                4、如果双方字符串都相等，就会开始比较长度
                */
                //把完成的秒速存下来, 与本地以保存的秒速做对比
                //先找到是哪种模式，在找是几成几的
                if (Sudoku_mode.equals("normalMode")) {//普通模式
                    if (type == 3) {
                        dataTime = "normalMode_data_time3";
                    } else if (type == 5) {
                        dataTime = "normalMode_data_time5";
                    } else if (type == 7) {
                        dataTime = "normalMode_data_time7";
                    } else if (type == 9) {
                        dataTime = "normalMode_data_time9";
                    }
                } else if (Sudoku_mode.equals("PrimeNumberMode")) {//素数模式
                    dataTime = "PrimeNumberMode_data_time5";
                } else if (Sudoku_mode.equals("OddNumberMode")) {//奇数模式
                    if (type == 3) {
                        dataTime = "OddNumberMode_data_time3";
                    } else if (type == 5) {
                        dataTime = "OddNumberMode_data_time5";
                    } else if (type == 7) {
                        dataTime = "OddNumberMode_data_time7";
                    }
                }


                //historyTime = prefData_time.getString(dataTime, "");
                getHistoryData(dataTime);


                //editor.apply();

                view.setBackgroundColor(getResources().getColor(R.color.common_google_signin_btn_text_dark_focused));
                view.setOnClickListener(null);
                stopTime();
                /*new AlertDialog.Builder(ClickInterfaceActivity.this)
                        .setMessage(dialogMessage)
                        .setPositiveButton("返回", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(ClickInterfaceActivity.this, SelectionModeActivity.class);
                                startActivity(intent);
                                ClickInterfaceActivity.this.finish();
                            }
                        })
                        .setNegativeButton("重新開始", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                setDatas();
                                ClickInterfaceActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        tableBorderLayout.setAdapter(new MyAdapter(ClickInterfaceActivity.this, numberList, clickItem, type), type);
                                        startTime();
                                    }
                                });
                            }
                        })
                        .setCancelable(false) //点击对话框外部是否关闭
                        .show();*/

            } else if (clickNumberList.get(clickNumber) == view.getId()) {
                view.setBackgroundDrawable(getResources().getDrawable(R.color.black));
                view.setOnClickListener(null);
                clickNumber++;
                countcount.setText(Integer.toString(clickNumberList.get(clickNumber)));
            }
        }
    };


    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    mlCount++;
                    int tatalSec = 0;
                    int msec = 0;
                    //精确到0.01秒
                    tatalSec = (int) (mlCount / 1000);
                    msec = (int) (mlCount % 1000);//毫秒
                    sec = (tatalSec % 1000);//秒
                    try {
                        time = String.format("%1$02d:%2$03d", sec, msec);
                        timetime.setText(time);
                        //Log.d(TAG, "" + time);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
            super.handleMessage(msg);
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
        stopTime();
        finish();
    }

    //读取历史记录
    private void getHistoryData(String datatime) {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(1000000, TimeUnit.SECONDS)   // 設置連線Timeout
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
                .build();

        //get連線
        Request request = new Request.Builder()
                .url("https://strokarebackend.herokuapp.com/focushistory/"+user_id+"/")
                //.header("Authorization", "Token "+token)
                .build();

        // 建立Call
        Call call = client.newCall(request);
        // 執行Call連線到網址
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 連線成功
                String result = response.body().string();
                try {

                    JSONObject Jobject = new JSONObject(result);
                    if(datatime.equals("normalMode_data_time3")){
                        historyTime =  Jobject.getString("normalMode_data_time3");
                    }
                    else if (datatime.equals("normalMode_data_time5")){
                        historyTime = Jobject.getString("normalMode_data_time5");
                    }
                    else if (datatime.equals("normalMode_data_time7")){
                        historyTime = Jobject.getString("normalMode_data_time7");
                    }
                    else if (datatime.equals("normalMode_data_time9")){
                        historyTime = Jobject.getString("normalMode_data_time9");
                    }
                    else if (datatime.equals("PrimeNumberMode_data_time5")){
                        historyTime = Jobject.getString("PrimeNumberMode_data_time5");
                    }
                    else if (datatime.equals("OddNumberMode_data_time3")){
                        historyTime = Jobject.getString("OddNumberMode_data_time3");
                    }
                    else if (datatime.equals("OddNumberMode_data_time5")){
                        historyTime = Jobject.getString("OddNumberMode_data_time5");
                    }
                    else if (datatime.equals("OddNumberMode_data_time7")){
                        historyTime = Jobject.getString("OddNumberMode_data_time7");
                    }


                    runOnUiThread(new Runnable() {
                        public void run() {
                            String dialogMessage = "";//需要显示的提示
                            if (historyTime != null && historyTime.length() > 0 && !historyTime.equals("暫無紀錄")) {//如果为空，就是本地没有数据
                                //对比本次时间和保存的时间
                                int contrast = time.compareTo(historyTime);
                                if (contrast < 0) { //小于表示 time 没 historyTime 大
                                    //editor.putString(dataTime, time);
                                    putHistoryData(datatime,time);
                                    dialogMessage = "恭喜，產生新紀錄：" + time + "\n上次紀錄為：" + historyTime;
                                } else {
                                    dialogMessage = "花費時間：" + time + "\n歷史最快紀錄為：" + historyTime;
                                }
                            } else {
                                //editor.putString(dataTime, time);
                                putHistoryData(datatime,time);
                                dialogMessage = "恭喜，產生新紀錄：" + time;
                            }
                            new AlertDialog.Builder(ClickInterfaceActivity.this)
                                    .setMessage(dialogMessage)
                                    .setPositiveButton("返回", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            Intent intent = new Intent(ClickInterfaceActivity.this, SelectionModeActivity.class);
                                            startActivity(intent);
                                            ClickInterfaceActivity.this.finish();
                                        }
                                    })
                                    .setNegativeButton("重新開始", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            setDatas();
                                            ClickInterfaceActivity.this.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Log.d(TAG, "重新開始: color："+ whetherColor);
                                                    if (whetherColor == true) {
                                                        myAdapter = new MyAdapter(ClickInterfaceActivity.this, numberList, clickItem, type, isR, isG, isB);
                                                        tableBorderLayout.setAdapter(myAdapter, type);
                                                        Log.d(TAG, "顏色!!!");
                                                    } else {
                                                        myAdapter = new MyAdapter(ClickInterfaceActivity.this, numberList, clickItem, type);
                                                        tableBorderLayout.setAdapter(myAdapter, type);
                                                        Log.d(TAG, "白色!!!");
                                                    }

                                                    //tableBorderLayout.setAdapter(new MyAdapter(ClickInterfaceActivity.this, numberList, clickItem, type), type);
                                                    startTime();
                                                }
                                            });
                                        }
                                    })
                                    .setCancelable(false) //点击对话框外部是否关闭
                                    .show();
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getApplicationContext(), "error :(", Toast.LENGTH_SHORT).show();
                        }
                    });
                }


            }

            @Override
            public void onFailure(Call call, IOException e) {
                // 連線失敗
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), "連線失敗，請稍後再試", Toast.LENGTH_SHORT).show();
                    }
                });





            }
        });
    }

    private void putHistoryData(String datatime, String time){
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(1000000, TimeUnit.SECONDS)   // 設置連線Timeout
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
                .build();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(datatime, time);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        // put your json here
        RequestBody formBody = RequestBody.create(JSON, jsonObject.toString());

        //get連線
        Request request = new Request.Builder()
                .url("https://strokarebackend.herokuapp.com/focushistory/"+user_id+"/")
                //.header("Authorization", "Token "+token)
                .put(formBody)
                .build();

        // 建立Call
        Call call = client.newCall(request);
        // 執行Call連線到網址
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 連線成功
                String result = response.body().string();

                try {
                    JSONObject Jobject = new JSONObject(result);
                    if(Jobject.getString("user").equals("0")){
                        runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(getApplicationContext(), "error :(", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getApplicationContext(), "error :(", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }

            @Override
            public void onFailure(Call call, IOException e) {
                // 連線失敗
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), "連線失敗，請稍後再試", Toast.LENGTH_SHORT).show();
                    }
                });





            }
        });
    }
}