package com.example.mytest.accelerometer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mytest.accelerometer.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class HistoryActivity extends AppCompatActivity {

    private static final String TAG = "Myhistory";
    private TextView tv_h_3X3, tv_h_5X5, tv_h_7X7, tv_h_9X9, tv_prime_5X5, tv_odd_3X3, tv_odd_5X5, tv_odd_7X7;
    private String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "My history open!!");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_history);
        //取得user_id
        SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = preferences.getString("user_id", "none");
        //質數模式的方格
        tv_prime_5X5 = findViewById(R.id.tv_prime_5X5);
        //機數模式的方格
        tv_odd_3X3 = findViewById(R.id.tv_odd_3X3);
        tv_odd_5X5 = findViewById(R.id.tv_odd_5X5);
        tv_odd_7X7 = findViewById(R.id.tv_odd_7X7);
        //普通模式的方格
        tv_h_3X3 = findViewById(R.id.tv_h_3X3);
        tv_h_5X5 = findViewById(R.id.tv_h_5X5);
        tv_h_7X7 = findViewById(R.id.tv_h_7X7);
        //tv_h_9X9 = findViewById(R.id.tv_h_9X9);

        setHistoryData();

    }

    private void setHistoryData() {

        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(1000000, TimeUnit.SECONDS)   // 設置連線Timeout
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
                .build();



        //get連線
        Request request = new Request.Builder()
                .url("https://strokarebackend.herokuapp.com/focushistory/"+ user_id +"/")
                //.header("Authorization", "Token "+token)
                .build();

        // 建立Call
        Call call = client.newCall(request);
        // 執行Call連線到網址
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 連線成功
                String result = response.body().string();

                try {
                    JSONObject Jobject = new JSONObject(result);
                    String data_time3 = Jobject.getString("normalMode_data_time3");
                    String data_time5 = Jobject.getString("normalMode_data_time5");
                    String data_time7 = Jobject.getString("normalMode_data_time7");
                    String data_time9 = Jobject.getString("normalMode_data_time9");
                    String data_Prime5 = Jobject.getString("PrimeNumberMode_data_time5");
                    String tv_odd3 = Jobject.getString("OddNumberMode_data_time3");
                    String tv_odd5 = Jobject.getString("OddNumberMode_data_time5");
                    String tv_odd7 = Jobject.getString("OddNumberMode_data_time7");

                    runOnUiThread(new Runnable() {
                        public void run() {
                            if (data_time3.length() > 0) {
                                tv_h_3X3.setText("" + data_time3);
                            } else {
                                tv_h_3X3.setText("暫無紀錄");
                            }


                            if (data_time5.length() > 0) {
                                tv_h_5X5.setText("" + data_time5);
                            } else {
                                tv_h_5X5.setText("暫無紀錄");
                            }


                            if (data_time7.length() > 0) {
                                tv_h_7X7.setText("" + data_time7);
                            } else {
                                tv_h_7X7.setText("暫無紀錄");
                            }


                            /*if (data_time9.length() > 0) {
                                tv_h_9X9.setText("" + data_time9);
                            } else {
                                tv_h_9X9.setText("暫無紀錄");
                            }*/


                            if (data_Prime5.length() > 0) {
                                tv_prime_5X5.setText("" + data_Prime5);
                            } else {
                                tv_prime_5X5.setText("暫無紀錄");
                            }


                            if (tv_odd3.length() > 0) {
                                tv_odd_3X3.setText("" + tv_odd3);
                            } else {
                                tv_odd_3X3.setText("暫無紀錄");
                            }


                            if (tv_odd5.length() > 0) {
                                tv_odd_5X5.setText("" + tv_odd5);
                            } else {
                                tv_odd_5X5.setText("暫無紀錄");
                            }


                            if (tv_odd7.length() > 0) {
                                tv_odd_7X7.setText("" + tv_odd7);
                            } else {
                                tv_odd_7X7.setText("暫無紀錄");
                            }
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getApplicationContext(), "error :(", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }

            @Override
            public void onFailure(Call call, IOException e) {
                // 連線失敗
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), "連線失敗，請稍後再試", Toast.LENGTH_SHORT).show();
                    }
                });





            }
        });
    }

}