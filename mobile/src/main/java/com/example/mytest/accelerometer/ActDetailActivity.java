package com.example.mytest.accelerometer;


import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;

import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class ActDetailActivity extends AppCompatActivity {
    private static final String TAG = "ActDetailActivity";

    FitnessOptions fitnessOptions;
    GoogleSignInAccount account;

    List<Entry> lineEntries = new ArrayList<>();

    private TextView mNameTv;
    private TextView mTimeTv;
    private TextView mTotalTimeTv;
    private TextView mCaloriesTv;
    private TextView mHeartAvgTv;
    private LineChart lineChart;

    String name;
    String total_time;
    Long start_time ;
    Long end_time ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_detail);

        Intent intent = this.getIntent();
        //取得傳遞過來的資料
        name = intent.getStringExtra("name");
        total_time = intent.getStringExtra("total_time");
        start_time = intent.getLongExtra("start_time",0);
        end_time = intent.getLongExtra("end_time",0);

        mNameTv = (TextView) findViewById(R.id.name);
        mTimeTv = (TextView) findViewById(R.id.time);
        mTotalTimeTv = (TextView) findViewById(R.id.total_time);
        mCaloriesTv = (TextView) findViewById(R.id.calories);
        mHeartAvgTv = (TextView) findViewById(R.id.heart_avg);
        lineChart = (LineChart) findViewById(R.id.lineChart);

        setFitnessOption();
        initLineChart();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onResume() {
        super.onResume();

        mNameTv.setText(name);
        mTotalTimeTv.setText("總時長 | "+total_time);

        SimpleDateFormat dtf_date = new SimpleDateFormat("yyyy/M/d");
        SimpleDateFormat dtf_time = new SimpleDateFormat("HH:mm");
        String date = dtf_date.format(start_time);
        String start_time_str = dtf_time.format(start_time);
        String end_time_str = dtf_time.format(end_time);
        mTimeTv.setText(date+" "+start_time_str+"~"+end_time_str);

        getCalories(start_time,end_time);
        showLineChart(start_time,end_time);
    }

    // ask for permission
    private void setFitnessOption() {
        fitnessOptions =
                FitnessOptions.builder()
                        .addDataType(DataType.AGGREGATE_CALORIES_EXPENDED, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.TYPE_HEART_RATE_BPM, FitnessOptions.ACCESS_READ)
                        .build();

        account = GoogleSignIn.getAccountForExtension(this, fitnessOptions);
    }

    //獲取卡路里
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getCalories(Long startTime, Long endTime){
        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(DataType. AGGREGATE_CALORIES_EXPENDED)
                .aggregate(DataType. TYPE_HEART_RATE_BPM)
                .bucketByTime(1, TimeUnit.DAYS)
                .enableServerQueries()
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();

        Fitness.getHistoryClient(this, account)
                .readData(readRequest)
                .addOnSuccessListener(response -> {
                    LinearLayout data_list = (LinearLayout) findViewById(R.id.activity);
                    // The aggregate query puts datasets into buckets, so convert to a single list of datasets
                    for (Bucket bucket : response.getBuckets()) {
                        for (DataSet dataSet : bucket.getDataSets()) {
                            for (DataPoint dp : dataSet.getDataPoints()) {
                                Log.e("History", "Data point:");
                                Log.e("History", "\tType: " + dp.getDataType().getName());
                                for (Field field : dp.getDataType().getFields()) {
                                    Log.e("History", "\tField: " + field.getName() +
                                            " Value: " + dp.getValue(field));
                                    switch( field.getName()){
                                        case "calories":
                                            double roundDbl = Math.round(dp.getValue(field).asFloat()*100.0)/100.0;
                                            mCaloriesTv.setText("消耗 | "+roundDbl+" 大卡");
                                            break;
                                        case "average":
                                            float heart_rate = dp.getValue(field).asFloat();
                                            mHeartAvgTv.setText("平均心率 | "+(int)heart_rate+" bpm");
                                            break;
                                        default :
                                            break;
                                    }
                                }
                            }
                        }
                    }

                })
                .addOnFailureListener(e ->
                        Log.w(TAG, "There was an error reading data from Google Fit", e));
    }

    // 初始化LineChart設定
    private void initLineChart(){
        lineChart.setNoDataText("查無活動資訊");   //// 如果没有數據的时候，會顯示這段文字
        lineChart.getDescription().setEnabled(false);  //不顯示description
        lineChart.getXAxis().setDrawGridLines(false);  //不顯示網格
        lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM); //X軸於下方顯示
        lineChart.getXAxis().setAxisMinimum(0f);
        lineChart.getAxisRight().setEnabled(false);  //不顯示左邊y軸
        lineChart.getAxisRight().setDrawGridLines(false); //不顯示網格
        lineChart.getAxisLeft().setDrawGridLines(false); //不顯示網格
        lineChart.setDragEnabled(false);  // 是否可以拖拽
        lineChart.setScaleEnabled(false);  // 是否可以缩放
        lineChart.setTouchEnabled(false);  // 是否可以觸摸
        lineChart.setExtraOffsets(5f,5f,5f,15f);

        Legend legend = lineChart.getLegend();
        legend.setTextColor(Color.GRAY); //設定Legend 文字顏色
        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        legend.setOrientation(Legend.LegendOrientation.HORIZONTAL);
    }

    //產生該日期之步數長條圖
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void showLineChart(Long startTime, Long endTime) {
        DataReadRequest readRequest = new DataReadRequest.Builder()
                .read(DataType.TYPE_HEART_RATE_BPM)
                .bucketByTime(1, TimeUnit.DAYS)
                .enableServerQueries()
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();


        Fitness.getHistoryClient(this, account)
                .readData(readRequest)
                .addOnSuccessListener(response -> {
                    lineEntries.clear();
                    Long startTime_min=TimeUnit.MILLISECONDS.toMinutes(startTime);
                    Long temp_time = 0l;
                    float total_heart = 0;
                    int num = 0;

                    // The aggregate query puts datasets into buckets, so convert to a single list of datasets
                    for (Bucket bucket : response.getBuckets()) {
                        for (DataSet dataSet : bucket.getDataSets()) {
                            for (DataPoint dp : dataSet.getDataPoints()) {
                                for (Field field : dp.getDataType().getFields()) {
                                    Log.e("History", "\tField: " + field.getName() +
                                            " Value: " + dp.getValue(field));

                                    float heart_rate = dp.getValue(field).asFloat();

                                    if(temp_time == dp.getStartTime(TimeUnit.MINUTES) || temp_time == 0l){
                                        total_heart += heart_rate;
                                        num ++;
                                    }
                                    else{
                                        lineEntries.add(new Entry(dp.getStartTime(TimeUnit.MINUTES)-startTime_min,(int)total_heart/num));  //紀錄該分鐘平均心率
                                        total_heart = 0;
                                        num = 0;
                                    }
                                    temp_time = dp.getStartTime(TimeUnit.MINUTES);
                                }
                            }
                        }
                    }
                    LineDataSet lineDataSet  = new LineDataSet (lineEntries, "該分鐘的心率(bpm)");  // y轴的一組數據集合
                    lineDataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER); //平滑曲線
                    lineDataSet.setDrawCircles(false);  //不標註每個點
                    lineDataSet.setDrawValues(false);   //不顯示數值
                    lineDataSet.setColor(Color.parseColor("#dcb988"));//線條顏色(芥末黃)
                    lineDataSet.setLineWidth(2f);//線條寬度
                    LineData lineData = new LineData(lineDataSet);

                    lineChart.notifyDataSetChanged();
                    lineChart.invalidate();  // refreshes chart
                    lineChart.setData(lineData);
                })
                .addOnFailureListener(e ->
                        Log.w(TAG, "There was an error reading data from Google Fit", e));


    }

}
