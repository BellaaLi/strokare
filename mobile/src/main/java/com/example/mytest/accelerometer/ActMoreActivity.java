package com.example.mytest.accelerometer;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessActivities;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.fitness.request.SessionReadRequest;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ActMoreActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{
    private static final String TAG = "ActMoreActivity";

    FitnessOptions fitnessOptions;
    GoogleSignInAccount account;
    Calendar calendar = Calendar.getInstance();

    private Button mPickBtn;
    private ImageButton mBackBtn;
    private ImageButton mNextBtn;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_act);
        mPickBtn = (Button) findViewById(R.id.pick);
        mBackBtn = (ImageButton) findViewById(R.id.back);
        mNextBtn = (ImageButton) findViewById(R.id.next);

        //按下按鈕，可呼叫Date Picker
        mPickBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });
        //前一天
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                getInfoByDate(calendar);
            }
        });
        //後一天
        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                getInfoByDate(calendar);
            }
        });

        setFitnessOption();
        getInfoByDate(calendar);
    }

    // ask for permission
    private void setFitnessOption() {
        fitnessOptions =
                FitnessOptions.builder()
                        .addDataType(DataType.TYPE_ACTIVITY_SEGMENT, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.AGGREGATE_CALORIES_EXPENDED, FitnessOptions.ACCESS_READ)
                        .build();

        account = GoogleSignIn.getAccountForExtension(this, fitnessOptions);
    }

    //Date Picker
    public void showDatePickerDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this,
                this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());  //限制日期範圍只到今天
        datePickerDialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = year + "年" + (month + 1) + "月" + dayOfMonth + "日";
        mPickBtn.setText(date);
        calendar.set(year, month, dayOfMonth);
        getInfoByDate(calendar);
    }

    // 呼叫選取日期的相關資訊
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getInfoByDate(Calendar calendar) {
        checkNextBtn(calendar);
        //改變mPickBtn文字為現在所選日期
        SimpleDateFormat dtf = new SimpleDateFormat("yyyy年M月d日");
        Date dateObj = calendar.getTime();
        String formattedDate = dtf.format(dateObj);
        mPickBtn.setText(formattedDate);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        //設定步數時間範圍
        ZonedDateTime startTime = LocalDate.of(year, month, dayOfMonth).atStartOfDay(ZoneId.systemDefault());
        ZonedDateTime endTime = LocalDate.of(year, month, dayOfMonth).atTime(23, 59, 59).atZone(ZoneId.systemDefault());
        Log.i(TAG, "Range Start: " + startTime);
        Log.i(TAG, "Range End: " + endTime);

        getActivity(startTime,endTime);
    }

    //獲取活動
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getActivity(ZonedDateTime startTime, ZonedDateTime endTime){

        SessionReadRequest readRequest = new SessionReadRequest.Builder()
                .read(DataType.TYPE_ACTIVITY_SEGMENT)
                .readSessionsFromAllApps()
                .enableServerQueries()
                .setTimeInterval(startTime.toEpochSecond(), endTime.toEpochSecond(), TimeUnit.SECONDS)
                .build();

        Fitness.getSessionsClient(this, account)
                .readSession(readRequest)
                .addOnSuccessListener (response -> {
                    SimpleDateFormat dtf = new SimpleDateFormat("HH:mm");

                    LinearLayout data_list = (LinearLayout) findViewById(R.id.activity);
                    //清空
                    data_list.removeAllViews();

                    for (Session session : response.getSessions()) {
                        //BUTTON//
                        Button mActBtn = new Button(this);
                        mActBtn.setTextSize(22);
                        mActBtn.setBackgroundColor(Color.TRANSPARENT);

                        LinearLayout.LayoutParams view_layout1 =  new LinearLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
                        mActBtn.setText("▪  "+session.getName());
                        mActBtn.setLayoutParams(view_layout1);

                        //TEXT//
                        TextView timeTV = new TextView(this);
                        LinearLayout.LayoutParams view_layout2 =  new LinearLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
                        view_layout2.bottomMargin = 50;
                        view_layout2.leftMargin = 120;
                        String start_time = dtf.format(session.getStartTime(TimeUnit.MILLISECONDS));
                        String end_time = dtf.format(session.getEndTime(TimeUnit.MILLISECONDS));
                        timeTV.setText(start_time+" ~ "+ end_time + " ("+secondToTime(session.getActiveTime(TimeUnit.SECONDS))+")");
                        timeTV.setLayoutParams(view_layout2);
                        timeTV.setTextColor( getResources().getColor(R.color.gray));

                        data_list.addView(mActBtn);
                        data_list.addView(timeTV);

                        // 設置按下[查看更多]按鈕後的動作(ActDetailActivity)
                        mActBtn.setOnClickListener(v -> {
                            Intent intent = new Intent();
                            intent.setClass(ActMoreActivity.this , ActDetailActivity.class);
                            intent.putExtra("name",session.getName());
                            intent.putExtra("total_time",secondToTime(session.getActiveTime(TimeUnit.SECONDS)));
                            intent.putExtra("start_time",session.getStartTime(TimeUnit.MILLISECONDS));
                            intent.putExtra("end_time",session.getEndTime(TimeUnit.MILLISECONDS));
                            startActivity(intent);
                        });
                    }
                    //若無資料(沒有盡到for迴圈)，則設定文字[查無資料]
                    if(data_list.getChildCount() == 0){
                        TextView noDataTV = new TextView(this);
                        noDataTV.setText("查無資料\n\n");
                        noDataTV.setTextSize(20);
                        noDataTV.setGravity(Gravity.CENTER);
                        data_list.addView(noDataTV);

                        ImageView sorryPic = new ImageView(this);
                        sorryPic.setImageResource(R.drawable.sorry);
                        sorryPic.setForegroundGravity(Gravity.CENTER);
                        data_list.addView(sorryPic);
                    }
                })
                .addOnFailureListener(e ->
                        Log.w(TAG, "There was an error reading data from Google Fit", e));
    }
    //秒數轉時間
    private String secondToTime(long second){
        long days = second / 86400;            //轉換天數
        second = second % 86400;            //剩余秒數
        long hours = second / 3600;            //轉換小時
        second = second % 3600;                //剩余秒數
        long minutes = second /60;            //轉換分鍾
        second = second % 60;                //剩余秒數
        if(days>0){
            return days + "天" + hours + "小時" + minutes + "分" + second + "秒";
        }else if(hours>0){
            return hours + "小時" + minutes + "分" + second + "秒";
        }else{
            return minutes + "分" + second + "秒";
        }
    }

    //如果按next會超過今天日期，則不能按next按鈕
    private void checkNextBtn(Calendar pick_cal) {
        if (pick_cal.get(Calendar.DAY_OF_YEAR) == Calendar.getInstance().get(Calendar.DAY_OF_YEAR)){
            mNextBtn.setEnabled(false);
            mNextBtn.setColorFilter(Color.TRANSPARENT);
        }
        else{
            mNextBtn.setEnabled(true);
            mNextBtn.setColorFilter(Color.DKGRAY);
        }
    }

}