package com.example.mytest.accelerometer;


import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Goal;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.request.GoalsReadRequest;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.TimeUnit;


public class WeekFragment extends Fragment implements DatePickerDialog.OnDateSetListener{
    private static final String TAG = "WeekFragment";

    FitnessOptions fitnessOptions;
    GoogleSignInAccount account;
    Calendar calendar = Calendar.getInstance();  //獲取今天日期
    ArrayList<BarEntry> barEntries = new ArrayList<>();

    BarChart barChart;
    private ImageButton mBackBtn;
    private ImageButton mNextBtn;
    private Button mPickBtn;
    private TextView mAverageText;

    int goal = 0;
    int max_step = 0;

    @Override
    @RequiresApi(api = Build.VERSION_CODES.O)
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_week, container, false);
        barChart = (BarChart) v.findViewById(R.id.barChart);
        mBackBtn = (ImageButton) v.findViewById(R.id.back);
        mNextBtn = (ImageButton) v.findViewById(R.id.next);
        mPickBtn = (Button) v.findViewById(R.id.pick);
        mAverageText = (TextView) v.findViewById(R.id.average);

        //按下按鈕，可呼叫Date Picker
        mPickBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });
        //上個禮拜
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                calendar.add(Calendar.WEEK_OF_YEAR, -1);
                getWeekInfo();
            }
        });
        //下個禮拜
        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                calendar.add(Calendar.WEEK_OF_YEAR, 1);
                getWeekInfo();
            }
        });

        setFitnessOption();
        initBarChart();
        getGoal();
        getWeekInfo();
        return v;
    }

    // ask for permission
    private void setFitnessOption() {
        fitnessOptions =
                FitnessOptions.builder()
                        .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.AGGREGATE_DISTANCE_DELTA, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.AGGREGATE_CALORIES_EXPENDED, FitnessOptions.ACCESS_READ)
                        .build();
        account = GoogleSignIn.getAccountForExtension(getActivity(), fitnessOptions);
    }

    //Date Picker
    public void showDatePickerDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                getActivity(),
                this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());  //限制日期範圍只到今天
        datePickerDialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = year + "年" + (month + 1) + "月" + dayOfMonth + "日";
        calendar.set(year, month, dayOfMonth);
        getWeekInfo();

    }

    //獲取該禮拜的資料
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void getWeekInfo() {
        SimpleDateFormat dtf = new SimpleDateFormat("M/d");
        int week = calendar.get(Calendar.DAY_OF_WEEK);

        switch (week) {
            case 2:  //星期一
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                break;
            case 3:  //星期二
                calendar.add(Calendar.DAY_OF_YEAR, -2);
                break;
            case 4:  //星期三
                calendar.add(Calendar.DAY_OF_YEAR, -3);
                break;
            case 5:  //星期四
                calendar.add(Calendar.DAY_OF_YEAR, -4);
                break;
            case 6:  //星期五
                calendar.add(Calendar.DAY_OF_YEAR, -5);
                break;
            case 7:  //星期六
                calendar.add(Calendar.DAY_OF_YEAR, -6);
                break;
            default:
                break;
        }

        Date dateObj1 = calendar.getTime();
        String formattedDate1 = dtf.format(dateObj1);
        calendar.add(Calendar.DAY_OF_YEAR, 6);
        Date dateObj2 = calendar.getTime();
        String formattedDate2 = dtf.format(dateObj2);
        mPickBtn.setText(formattedDate1 + "~" + formattedDate2);

        checkNextBtn(calendar);
        getInfoByDate(calendar);
    }

    // 呼叫選取日期的相關資訊
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getInfoByDate(Calendar calendar) {
        //設定結束時間
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        ZonedDateTime endTime = LocalDateTime.of(year, month, dayOfMonth,23,59,59).atZone(ZoneId.systemDefault());
        //設定開始時間
        calendar.add(Calendar.DAY_OF_YEAR, -6);
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        ZonedDateTime startTime = LocalDate.of(year, month, dayOfMonth).atStartOfDay(ZoneId.systemDefault());
        //印出時間範圍
        Log.i(TAG, "Range Start: " + startTime);
        Log.i(TAG, "Range End: " + endTime);

        showBarChart(startTime, endTime);
    }

    //產生該日期之步數長條圖
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void showBarChart(ZonedDateTime startTime, ZonedDateTime endTime) {
        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(DataType.AGGREGATE_STEP_COUNT_DELTA)
                .bucketByTime(1, TimeUnit.DAYS)
                .enableServerQueries()
                .setTimeRange(startTime.toEpochSecond(), endTime.toEpochSecond(), TimeUnit.SECONDS)
                .build();


        Fitness.getHistoryClient(getActivity(), account)
                .readData(readRequest)
                .addOnSuccessListener(response -> {
                    int total = 0;
                    int[] value_arr = new int[24];
                    int i = 0;
                    SimpleDateFormat dateTimeFormat = new SimpleDateFormat("EEE", Locale.ENGLISH);
                    // The aggregate query puts datasets into buckets, so convert to a single list of datasets
                    for (Bucket bucket : response.getBuckets()) {
                        for (DataSet dataSet : bucket.getDataSets()) {
                            for (DataPoint dp : dataSet.getDataPoints()) {
                                for (Field field : dp.getDataType().getFields()) {
                                    Log.e("History", "\tField: " + field.getName() +
                                            " Value: " + dp.getValue(field));
                                    total += dp.getValue(field).asInt();
                                    String label = dateTimeFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS));
                                    switch (label) {
                                        case "Sun":  //星期日
                                            i = 0;
                                            break;
                                        case "Mon":  //星期一
                                            i = 1;
                                            break;
                                        case "Tue":  //星期二
                                            i = 2;
                                            break;
                                        case "Wed":  //星期三
                                            i = 3;
                                            break;
                                        case "Thu":  //星期四
                                            i = 4;
                                            break;
                                        case "Fri":  //星期五
                                            i = 5;
                                            break;
                                        case "Sat":  //星期六
                                            i = 6;
                                            break;
                                        default:
                                            break;
                                    }
                                    value_arr[i] = dp.getValue(field).asInt();
                                }
                            }
                        }
                    }
                    mAverageText.setText("每日平均步數:  " + (total / 7));
                    addBarChart(value_arr);
                })
                .addOnFailureListener(e ->
                        Log.w(TAG, "There was an error reading data from Google Fit", e));
    }

    // 設定BarChart資料
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void addBarChart(int[] value_arr) {
        barEntries.clear();
        max_step = 0;
        for (int i = 0; i< 24; i++) {
            if(value_arr[i] != 0){
                barEntries.add(new BarEntry(i, value_arr[i]));
            }
            if (value_arr[i] > max_step) {
                max_step = value_arr[i];
                Log.e("test", ""+max_step);
            }
        }


        if(goal>=0){
            //為保證目標線會畫出來
            if (goal > max_step) {
                barChart.getAxisLeft().setAxisMaximum(goal + 20);
            }
            else{
                barChart.getAxisLeft().setAxisMaximum(max_step + 20);
            }
            //設定目標線
            LimitLine goalLine = new LimitLine(goal, "goal: " + goal);
            goalLine.enableDashedLine(10f, 10f, 0f);  //允許在虛線模式下繪製(線段長度，分隔長度，偏移量)
            goalLine.setLineColor(Color.parseColor("#df957f"));
            goalLine.setTextSize(10f);
            goalLine.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);//标签位置
            barChart.getAxisLeft().removeAllLimitLines();;   //刪除原本的限制線
            barChart.getAxisLeft().addLimitLine(goalLine);   //新增限制線
        }

        //加入數據
        BarDataSet barDataSet = new BarDataSet(barEntries, "step");  // y轴的一組數據集合
        barDataSet.setColor(Color.parseColor("#acc3bf")); //更換顏色
        ArrayList<IBarDataSet> iBarDataSets = new ArrayList<>();   // y轴的全部數據集合
        iBarDataSets.add(barDataSet);
        BarData barData = new BarData(iBarDataSets);
        barData.setValueFormatter(new MyValueFormatter());  //自訂數值格式

        barChart.notifyDataSetChanged();
        barChart.invalidate();  // refreshes chart
        barChart.setData(barData);
    }

    //將圖表中的值轉為整數
    public class MyValueFormatter extends ValueFormatter {
        @Override
        public String getFormattedValue(float value) {
            return "" + ((int) value);
        }
    }


    // 初始化BarChart設定
    private void initBarChart() {
        String[] labelWeekdayNames = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};

        barChart.setNoDataText("查無活動資訊");   //// 如果没有數據的时候，會顯示這段文字

        //****x軸 labsl****//
        barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(labelWeekdayNames));
        barChart.getXAxis().setLabelCount(6);
        barChart.getXAxis().setAxisMinimum(-0.5f);
        barChart.getXAxis().setAxisMaximum(6.5f);
        //****************//

        barChart.getAxisLeft().setAxisMinimum(0); //設置y軸座標的最小值
        barChart.getDescription().setEnabled(false);  //不顯示description
        barChart.getLegend().setEnabled(false);     //不顯示legend
        barChart.getXAxis().setDrawGridLines(false);  //不顯示網格
        barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM); //X軸於下方顯示
        barChart.getAxisRight().setEnabled(false);  //不顯示右邊y軸

        //******左y軸******//
        barChart.getAxisLeft().setDrawLabels(false); // y軸 no axis labels
        barChart.getAxisLeft().setDrawAxisLine(false); // y軸 no axis line
        barChart.getAxisLeft().setDrawGridLines(false); //y軸 no grid lines
        barChart.getAxisLeft().setDrawZeroLine(true); // y軸 draw a zero line
        //****************//


        barChart.setDrawValueAboveBar(true);  //柱狀圖上面的數值顯示在柱子上面還是柱子裡面
        barChart.setDragEnabled(false);  // 是否可以拖拽
        barChart.setScaleEnabled(false);  // 是否可以缩放
        barChart.setTouchEnabled(false);  // 是否可以觸摸
    }

    //得到目標
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getGoal() {
        GoalsReadRequest goalsReadRequest = new GoalsReadRequest.Builder()
                .addDataType(DataType.TYPE_STEP_COUNT_DELTA)
                .build();

        Fitness.getGoalsClient(getActivity(), account)
                .readCurrentGoals(goalsReadRequest)
                .addOnSuccessListener(goals -> {
                    Optional<Goal> optionalGoal = goals.stream().findFirst();
                    if(optionalGoal.isPresent()){
                        goal = (int)optionalGoal.get().getMetricObjective().getValue();
                    }
                    else{
                        goal = -1;
                    }
                });
    }

    //如果按next會超過今天日期，則不能按next按鈕
    private void checkNextBtn(Calendar pick_cal) {
        if (pick_cal.get(Calendar.DAY_OF_YEAR) >= Calendar.getInstance().get(Calendar.DAY_OF_YEAR)){
            mNextBtn.setEnabled(false);
            mNextBtn.setColorFilter(Color.TRANSPARENT);
        }
        else{
            mNextBtn.setEnabled(true);
            mNextBtn.setColorFilter(Color.DKGRAY);
        }
    }


}
