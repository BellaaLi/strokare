package com.example.mytest.accelerometer;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class InitialFragment extends Fragment implements View.OnClickListener{

    Button to_login_Btn;
    Button to_reg_Btn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.activity_initial, container, false);
        getActivity().setTitle("");
        to_login_Btn = rootView.findViewById(R.id.go_to_login_button);
        to_reg_Btn = rootView.findViewById(R.id.go_to_registration_button);
        //按鈕加入監聽事件
        to_login_Btn.setOnClickListener(this);
        to_reg_Btn.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.go_to_login_button:
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, new Login()).addToBackStack(null).commit();
                break;
            case R.id.go_to_registration_button:
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, new Register()).addToBackStack(null).commit();
                break;
        }
    }
}