package com.example.mytest.accelerometer;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.charts.BarChart;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class HeartMoreActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    private static final String TAG = "HeartMoreActivity";

    FitnessOptions fitnessOptions;
    GoogleSignInAccount account;
    Calendar calendar = Calendar.getInstance();

    private Button mPickBtn;
    private ImageButton mBackBtn;
    private ImageButton mNextBtn;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_heart_info);
        mPickBtn = (Button) findViewById(R.id.pick);
        mBackBtn = (ImageButton) findViewById(R.id.back);
        mNextBtn = (ImageButton) findViewById(R.id.next);

        //按下按鈕，可呼叫Date Picker
        mPickBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });
        //前一天
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                getInfoByDate(calendar);
            }
        });
        //後一天
        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                getInfoByDate(calendar);
            }
        });

        setFitnessOption();
        getInfoByDate(calendar);
    }

    // ask for permission
    private void setFitnessOption() {
        fitnessOptions =
                FitnessOptions.builder()
                        .addDataType(DataType.TYPE_HEART_RATE_BPM, FitnessOptions.ACCESS_READ)
                        .build();

        account = GoogleSignIn.getAccountForExtension(this, fitnessOptions);
    }

    //Date Picker
    public void showDatePickerDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this,
                this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());  //限制日期範圍只到今天
        datePickerDialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = year + "年" + (month + 1) + "月" + dayOfMonth + "日";
        mPickBtn.setText(date);
        calendar.set(year, month, dayOfMonth);
        getInfoByDate(calendar);
    }

    // 呼叫選取日期的相關資訊
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getInfoByDate(Calendar calendar) {
        checkNextBtn(calendar);
        //改變mPickBtn文字為現在所選日期
        SimpleDateFormat dtf = new SimpleDateFormat("yyyy年M月d日");
        Date dateObj = calendar.getTime();
        String formattedDate = dtf.format(dateObj);
        mPickBtn.setText(formattedDate);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        //設定步數時間範圍
        ZonedDateTime startTime = LocalDate.of(year, month, dayOfMonth).atStartOfDay(ZoneId.systemDefault());
        ZonedDateTime endTime = LocalDate.of(year, month, dayOfMonth).atTime(23, 59, 59).atZone(ZoneId.systemDefault());
        Log.i(TAG, "Range Start: " + startTime);
        Log.i(TAG, "Range End: " + endTime);

        getSummary(startTime,endTime);
        getHeartData(startTime,endTime);
    }

    //獲取心率統計資料
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getSummary(ZonedDateTime startTime, ZonedDateTime endTime){

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(DataType. TYPE_HEART_RATE_BPM)
                .bucketByTime(1, TimeUnit.DAYS)
                .enableServerQueries()
                .setTimeRange(startTime.toEpochSecond(), endTime.toEpochSecond(), TimeUnit.SECONDS)
                .build();

        Fitness.getHistoryClient(this, account)
                .readData(readRequest)
                .addOnSuccessListener(response -> {
                    TableLayout data_list = (TableLayout)findViewById(R.id.heart_list); data_list = (TableLayout)findViewById(R.id.heart_summary);
                    //清空
                    data_list.removeAllViews();

                    //TableLayout設定
                    data_list.setStretchAllColumns(true);
                    TableLayout.LayoutParams row_layout = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);

                    // The aggregate query puts datasets into buckets, so convert to a single list of datasets
                    for (Bucket bucket : response.getBuckets()) {
                        for (DataSet dataSet : bucket.getDataSets()) {
                            for (DataPoint dp : dataSet.getDataPoints()) {
                                Log.e("History", "Data point:");
                                Log.e("History", "\tType: " + dp.getDataType().getName());
                                for (Field field : dp.getDataType().getFields()) {
                                    Log.e("History", "\tField: " + field.getName() +
                                            " Value: " + dp.getValue(field));

                                    TableRow tr = new TableRow(this);
                                    tr.setLayoutParams(row_layout);
                                    tr.setGravity(Gravity.CENTER_HORIZONTAL);

                                    TextView itemTV = new TextView(this);
                                    TextView valueTV = new TextView(this);
                                    TableRow.LayoutParams view_layout =  new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 0.1f);
                                    itemTV.setTextColor(getResources().getColor(R.color.gray));
                                    valueTV.setTextColor(getResources().getColor(R.color.gray));

                                    switch( field.getName()){
                                        case "max":
                                            itemTV.setText("最大值");
                                            itemTV.setLayoutParams(view_layout);
                                            break;

                                        case "min":
                                            itemTV.setText("最小值");
                                            itemTV.setLayoutParams(view_layout);
                                            break;
                                        default :
                                            itemTV.setText("平均值");
                                            itemTV.setLayoutParams(view_layout);
                                            break;
                                    }
                                    float heart_rate = dp.getValue(field).asFloat();
                                    valueTV.setText(""+(int)heart_rate);
                                    valueTV.setLayoutParams(view_layout);

                                    tr.addView(itemTV);
                                    tr.addView(valueTV);
                                    data_list.addView(tr);
                                }
                            }
                        }
                    }
                    //若無資料(沒有盡到for迴圈)，則設定文字[查無資料]
                    if(data_list.getChildCount() == 0){
                        TableRow tr = new TableRow(this);
                        tr.setLayoutParams(row_layout);
                        tr.setGravity(Gravity.CENTER_HORIZONTAL);
                        TextView noDataTV = new TextView(this);
                        noDataTV.setText("查無資料");
                        noDataTV.setTextColor(getResources().getColor(R.color.gray));
                        tr.addView(noDataTV);
                        data_list.addView(tr);
                    }
                })
                .addOnFailureListener(e ->
                        Log.w(TAG, "There was an error reading data from Google Fit", e));
    }

    // 查看今日心率所有筆數
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getHeartData(ZonedDateTime startTime, ZonedDateTime endTime) {
        Log.i("HEART", "Range Start: " + startTime);
        Log.i("HEART", "Range End: " + endTime);

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .read(DataType.TYPE_HEART_RATE_BPM)
                .bucketByTime(1, TimeUnit.DAYS)
                .enableServerQueries()
                .setTimeRange(startTime.toEpochSecond(), endTime.toEpochSecond(), TimeUnit.SECONDS)
                .build();

        Fitness.getHistoryClient(this, account)
                .readData(readRequest)
                .addOnSuccessListener (response -> {
                    TableLayout data_list = (TableLayout)findViewById(R.id.heart_list); data_list = (TableLayout)findViewById(R.id.heart_list);
                    //清空
                    data_list.removeAllViews();

                    //設定
                    data_list.setStretchAllColumns(true);
                    TableLayout.LayoutParams row_layout = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);

                    SimpleDateFormat dtf = new SimpleDateFormat("HH:mm");

                    for (Bucket bucket : response.getBuckets()) {
                        for (DataSet dataSet : bucket.getDataSets()) {
                            for (DataPoint dp : dataSet.getDataPoints()) {
                                for (Field field : dp.getDataType().getFields()) {
                                    Log.e("History", "\tField: " + field.getName() +
                                            " Value: " + dp.getValue(field));

                                    TableRow tr = new TableRow(this);
                                    tr.setLayoutParams(row_layout);
                                    tr.setGravity(Gravity.CENTER_HORIZONTAL);

                                    TextView heartTV = new TextView(this);
                                    float heart_rate = dp.getValue(field).asFloat();
                                    heartTV.setText(""+(int)heart_rate);
                                    heartTV.setTextColor(getResources().getColor(R.color.gray));
                                    //設定權重
                                    TableRow.LayoutParams view_layout =  new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 0.15f);
                                    heartTV.setLayoutParams(view_layout);

                                    TextView bpmTV = new TextView(this);
                                    bpmTV.setText("bpm");
                                    bpmTV.setTextColor(getResources().getColor(R.color.gray));
                                    //設定權重
                                    view_layout = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 0.35f);
                                    bpmTV.setLayoutParams(view_layout);

                                    TextView timeTV = new TextView(this);
                                    timeTV.setText(dtf.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                                    timeTV.setTextColor(getResources().getColor(R.color.gray));
                                    //設定權重
                                    view_layout = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 0.5f);
                                    timeTV.setLayoutParams(view_layout);

                                    tr.addView(heartTV);
                                    tr.addView(bpmTV);
                                    tr.addView(timeTV);
                                    data_list.addView(tr);
                                }
                            }
                        }
                    }
                    //若無資料(沒有盡到for迴圈)，則設定文字[查無資料]
                    if(data_list.getChildCount() == 0){
                        TableRow tr = new TableRow(this);
                        tr.setLayoutParams(row_layout);
                        tr.setGravity(Gravity.CENTER_HORIZONTAL);
                        TextView noDataTV = new TextView(this);
                        noDataTV.setText("查無資料");
                        noDataTV.setTextColor(getResources().getColor(R.color.gray));
                        tr.addView(noDataTV);
                        data_list.addView(tr);

                        ImageView sorryPic = new ImageView(this);
                        sorryPic.setImageResource(R.drawable.sorry);
                        sorryPic.setForegroundGravity(Gravity.CENTER);
                        data_list.addView(sorryPic);
                    }
                })
                .addOnFailureListener(e ->
                        Log.w(TAG, "There was an error reading data from Google Fit", e));
    }


    //如果按next會超過今天日期，則不能按next按鈕
    private void checkNextBtn(Calendar pick_cal) {
        if (pick_cal.get(Calendar.DAY_OF_YEAR) == Calendar.getInstance().get(Calendar.DAY_OF_YEAR)){
            mNextBtn.setEnabled(false);
            mNextBtn.setColorFilter(Color.TRANSPARENT);
        }
        else{
            mNextBtn.setEnabled(true);
            mNextBtn.setColorFilter(Color.DKGRAY);
        }
    }
}
