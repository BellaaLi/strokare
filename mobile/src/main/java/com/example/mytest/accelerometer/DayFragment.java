package com.example.mytest.accelerometer;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class DayFragment extends Fragment implements DatePickerDialog.OnDateSetListener {
    private static final String TAG = "DayFragment";

    FitnessOptions fitnessOptions;
    GoogleSignInAccount account;
    ArrayList<BarEntry> barEntries = new ArrayList<>();
    Calendar calendar = Calendar.getInstance();

    private Button mPickBtn;
    private ImageButton mBackBtn;
    private ImageButton mNextBtn;
    private TextView mStepText;
    private TextView mDistanceText;
    private TextView mCaloriesText;
    private BarChart barChart;



    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_day, container, false);
        mPickBtn = (Button) v.findViewById(R.id.pick);
        mBackBtn = (ImageButton)v.findViewById(R.id.back);
        mNextBtn = (ImageButton)v.findViewById(R.id.next);
        mStepText = (TextView) v.findViewById(R.id.step);
        mDistanceText = (TextView) v.findViewById(R.id.distance);
        mCaloriesText = (TextView) v.findViewById(R.id.calories);
        barChart = (BarChart) v.findViewById(R.id.barChart);
        initBarChart();
        //按下按鈕，可呼叫Date Picker
        mPickBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });
        //前一天
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                calendar.add(Calendar.DAY_OF_YEAR,-1);
                getInfoByDate(calendar);
            }
        });
        //後一天
        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                calendar.add(Calendar.DAY_OF_YEAR,1);
                getInfoByDate(calendar);
            }
        });

        setFitnessOption();
        getInfoByDate(calendar);
        return v;
    }

    // ask for permission
    private void setFitnessOption() {
        fitnessOptions =
                FitnessOptions.builder()
                        .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.AGGREGATE_DISTANCE_DELTA, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.AGGREGATE_CALORIES_EXPENDED, FitnessOptions.ACCESS_READ)
                        .build();
        account = GoogleSignIn.getAccountForExtension(getActivity(), fitnessOptions);
    }

    //Date Picker
    public void showDatePickerDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                getActivity(),
                this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());  //限制日期範圍只到今天
        datePickerDialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = year + "年" + (month + 1) + "月" + dayOfMonth + "日";
        mPickBtn.setText(date);
        calendar.set(year, month, dayOfMonth);
        getInfoByDate(calendar);

    }

    // 呼叫選取日期的相關資訊
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getInfoByDate(Calendar calendar) {
        checkNextBtn(calendar);
        //改變mPickBtn文字為現在所選日期
        SimpleDateFormat dtf = new SimpleDateFormat("yyyy年M月d日");
        Date dateObj = calendar.getTime();
        String formattedDate = dtf.format(dateObj);
        mPickBtn.setText(formattedDate);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        //設定步數時間範圍
        ZonedDateTime startTime = LocalDate.of(year, month, dayOfMonth).atStartOfDay(ZoneId.systemDefault());
        ZonedDateTime endTime = LocalDate.of(year, month, dayOfMonth).atTime(23, 59, 59).atZone(ZoneId.systemDefault());
        Log.i(TAG, "Range Start: " + startTime);
        Log.i(TAG, "Range End: " + endTime);

        showBarChart(startTime,endTime);
        getDistance(startTime,endTime);
        getCalories(startTime,endTime);
    }

    //產生該日期之步數長條圖
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void showBarChart(ZonedDateTime startTime, ZonedDateTime endTime) {
        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(DataType.AGGREGATE_STEP_COUNT_DELTA)
                .bucketByTime(1, TimeUnit.HOURS)
                .enableServerQueries()
                .setTimeRange(startTime.toEpochSecond(), endTime.toEpochSecond(), TimeUnit.SECONDS)
                .build();


        Fitness.getHistoryClient(getActivity(), account)
                .readData(readRequest)
                .addOnSuccessListener(response -> {
                    int total = 0;
                    int[] value_arr = new int[24];
                    long[] time_arr = new long[24];
                    int i = 0;
                    // The aggregate query puts datasets into buckets, so convert to a single list of datasets
                    for (Bucket bucket : response.getBuckets()) {
                        for (DataSet dataSet : bucket.getDataSets()) {
                            for (DataPoint dp : dataSet.getDataPoints()) {
                                for (Field field : dp.getDataType().getFields()) {
                                    Log.e("History", "\tField: " + field.getName() +
                                            " Value: " + dp.getValue(field));
                                    total += dp.getValue(field).asInt();
                                    value_arr[i] = dp.getValue(field).asInt();
                                    time_arr[i] = dp.getStartTime(TimeUnit.MILLISECONDS);
                                    i++;
                                }
                            }
                        }
                    }
                    mStepText.setText("該日總步數:  " + total);
                    addBarChart(value_arr,time_arr);
                })
                .addOnFailureListener(e ->
                        Log.w(TAG, "There was an error reading data from Google Fit", e));


    }

    // 設定BarChart資料
    private void addBarChart(int[] value_arr,long[] time_arr){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH");
        int hoursOfDay;
        barEntries.clear();
        for (int i = 0; value_arr[i]>0 ; i++) {
            //將毫秒轉為該天的幾點鐘
            hoursOfDay = Integer.parseInt(simpleDateFormat.format(time_arr[i]));
            barEntries.add(new BarEntry(hoursOfDay,value_arr[i]));
        }

        BarDataSet barDataSet = new BarDataSet(barEntries, "step");  // y轴的一組數據集合
        barDataSet.setColor(Color.parseColor("#acc3bf")); //更換顏色
        ArrayList<IBarDataSet> iBarDataSets = new ArrayList<>();   // y轴的全部數據集合
        iBarDataSets.add(barDataSet);
        BarData barData = new BarData(iBarDataSets);
        barData.setValueFormatter(new MyValueFormatter());  //自訂數值格式

        barChart.notifyDataSetChanged();
        barChart.invalidate();  // refreshes chart
        barChart.setData(barData);
    }

    //將圖表中的值轉為整數
    public class MyValueFormatter extends ValueFormatter {
        @Override
        public String getFormattedValue(float value) {
            return "" + ((int) value);
        }
    }

    // 初始化BarChart設定
    private void initBarChart(){
        barChart.setNoDataText("查無活動資訊");   //// 如果没有數據的时候，會顯示這段文字
        barChart.getXAxis().setLabelCount(12); //設置x軸顯示的標籤個數
        barChart.getXAxis().setAxisMinimum(-0.5f); //設置x軸座標的最小值
        barChart.getXAxis().setAxisMaximum(23.5f);  //設置x軸座標的最大值
        barChart.getAxisLeft().setAxisMinimum(0); //設置y軸座標的最小值
        barChart.getDescription().setEnabled(false);  //不顯示description
        barChart.getLegend().setEnabled(false);     //不顯示legend
        barChart.getXAxis().setDrawGridLines(false);  //不顯示網格
        barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM); //X軸於下方顯示
        barChart.getAxisRight().setEnabled(false);  //不顯示右邊y軸
        barChart.getAxisLeft().setEnabled(false);  //不顯示左邊y軸
        barChart.setDrawValueAboveBar(true);  //柱狀圖上面的數值顯示在柱子上面還是柱子裡面
        barChart.setDragEnabled(false);  // 是否可以拖拽
        barChart.setScaleEnabled(false);  // 是否可以缩放
        barChart.setTouchEnabled(false);  // 是否可以觸摸
    }

    //獲取距離
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getDistance(ZonedDateTime startTime, ZonedDateTime endTime){
        mDistanceText.setText("距離 | " +  0 + "公里");

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(DataType. AGGREGATE_DISTANCE_DELTA)
                .bucketByTime(1, TimeUnit.DAYS)
                .enableServerQueries()
                .setTimeRange(startTime.toEpochSecond(), endTime.toEpochSecond(), TimeUnit.SECONDS)
                .build();

        Fitness.getHistoryClient(getActivity(), account)
                .readData(readRequest)
                .addOnSuccessListener(response -> {
                    // The aggregate query puts datasets into buckets, so convert to a single list of datasets
                    for (Bucket bucket : response.getBuckets()) {
                        for (DataSet dataSet : bucket.getDataSets()) {
                            for (DataPoint dp : dataSet.getDataPoints()) {
                                Log.e("History", "Data point:");
                                Log.e("History", "\tType: " + dp.getDataType().getName());
                                for (Field field : dp.getDataType().getFields()) {
                                    Log.e("History", "\tField: " + field.getName() +
                                            " Value: " + dp.getValue(field));
                                    double distance = dp.getValue(field).asFloat()/1000.0;
                                    double roundDbl = Math.round(distance*100.0)/100.0;
                                    mDistanceText.setText("距離 | " +  roundDbl + "公里");
                                }
                            }
                        }
                    }

                })
                .addOnFailureListener(e ->
                        Log.w(TAG, "There was an error reading data from Google Fit", e));
    }
    //獲取卡路里
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getCalories(ZonedDateTime startTime, ZonedDateTime endTime){
        mCaloriesText.setText("燃燒 | " +  0 + "大卡");

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(DataType. AGGREGATE_CALORIES_EXPENDED)
                .bucketByTime(1, TimeUnit.DAYS)
                .enableServerQueries()
                .setTimeRange(startTime.toEpochSecond(), endTime.toEpochSecond(), TimeUnit.SECONDS)
                .build();

        Fitness.getHistoryClient(getActivity(), account)
                .readData(readRequest)
                .addOnSuccessListener(response -> {
                    // The aggregate query puts datasets into buckets, so convert to a single list of datasets
                    for (Bucket bucket : response.getBuckets()) {
                        for (DataSet dataSet : bucket.getDataSets()) {
                            for (DataPoint dp : dataSet.getDataPoints()) {
                                Log.e("History", "Data point:");
                                Log.e("History", "\tType: " + dp.getDataType().getName());
                                for (Field field : dp.getDataType().getFields()) {
                                    Log.e("History", "\tField: " + field.getName() +
                                            " Value: " + dp.getValue(field));
                                    double roundDbl = Math.round(dp.getValue(field).asFloat()*100.0)/100.0;

                                    //通過Spannable對象設置textview的樣式
                                    SpannableStringBuilder builder = new SpannableStringBuilder();
                                    SpannableString str1= new SpannableString("燃燒 | " +  roundDbl + "大卡");
                                    builder.append(str1);
                                    SpannableString str2= new SpannableString("\n(此數據包含所有活動及休息時的消耗能量)");
                                    str2.setSpan(new ForegroundColorSpan(Color.GRAY), 0, str2.length(), 0);
                                    str2.setSpan(new AbsoluteSizeSpan(12,true), 0, str2.length(), 0);
                                    builder.append(str2);

                                    mCaloriesText.setText( builder, TextView.BufferType.SPANNABLE);
                                }
                            }
                        }
                    }

                })
                .addOnFailureListener(e ->
                        Log.w(TAG, "There was an error reading data from Google Fit", e));
    }

    //如果按next會超過今天日期，則不能按next按鈕
    private void checkNextBtn(Calendar pick_cal) {
        if (pick_cal.get(Calendar.DAY_OF_YEAR) == Calendar.getInstance().get(Calendar.DAY_OF_YEAR)){
            mNextBtn.setEnabled(false);
            mNextBtn.setColorFilter(Color.TRANSPARENT);
        }
        else{
            mNextBtn.setEnabled(true);
            mNextBtn.setColorFilter(Color.DKGRAY);
        }
    }


}