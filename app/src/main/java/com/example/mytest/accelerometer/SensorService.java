package com.example.mytest.accelerometer;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.firebase.client.Firebase;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.wearable.MessageClient;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class SensorService extends Service implements
        SensorEventListener{
    private static final String TAG = "SensorService";

    //紀錄有無啟動
    private static boolean isStart ;

    // 儲存資料取樣頻率1000ms(1秒)
    private static final int ACCE_FILTER_DATA_MIN_TIME = 1000;
    //最後一次儲存的時間
    long lastSaved_accelerometer = 0;
    long lastSaved_gyroscope = 0;
    //此次執行程式的日期
    SimpleDateFormat d_format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    String dt = d_format.format(new Date());
    //連線到資料庫
    FirebaseDatabase db= FirebaseDatabase.getInstance("https://accelerometer-becfb-default-rtdb.asia-southeast1.firebasedatabase.app//");
    //取得watch_ID
    String watch_ID=getSerialNumber();
    //根目錄(watch_ID)
    DatabaseReference idRef = db.getReference(watch_ID);
    //日期目錄
    DatabaseReference dateRef = idRef.child(dt);

    private SensorManager mSensorManager = null;
    private Sensor mAccelerometer;
    private Sensor mGyroscope;
    private String mConnectedNode;

    private String hand;  //紀錄配戴於哪隻手以改變path
    private String action;  //紀錄要執行的動作

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        //離線可使用
        Firebase.setAndroidContext(this);
        Firebase.getDefaultConfig().setPersistenceEnabled(true);

        mSensorManager.registerListener(this, mAccelerometer,mSensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mGyroscope,mSensorManager.SENSOR_DELAY_NORMAL);
        isStart = true;

        hand = intent.getStringExtra("hand");
        Log.d(TAG, "hand: " + hand);

        action = intent.getStringExtra("action");
        Log.d(TAG, "action: " + action);

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);


        // 避免自動關閉service
        showNotification();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");

        mSensorManager.unregisterListener(this, mAccelerometer);
        mSensorManager.unregisterListener(this, mGyroscope);
        isStart = false;
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // do nothing
    }

    int i = 0;
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        // grab the values and timestamp
        //accelerometer
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            if ((System.currentTimeMillis() - lastSaved_accelerometer) >= ACCE_FILTER_DATA_MIN_TIME) {
                //時間戳 timeStamp
                long timeStamp = sensorEvent.timestamp;
                long timeInMillis = (System.currentTimeMillis()
                        + (timeStamp - SystemClock.elapsedRealtimeNanos()) / 1000000L);
                //將時間戳轉為日期
                String date = stampToDate(timeInMillis);

                lastSaved_accelerometer = System.currentTimeMillis();
                float ax = sensorEvent.values[0];
                float ay = sensorEvent.values[1];
                float az = sensorEvent.values[2];

                Log.d(TAG, "onSensorChanged_accelerometer: aX:" + ax + " aY: " + ay + " aZ: " + az + " " + date);

                //傳送到firebase
                if(action.equals("firebase") || action.equals("both")){
                    DatabaseReference childRef = dateRef.child(String.valueOf(i));
                    Map<String, Object> dataRedUpdates = new HashMap<>();
                    //將ax,ay,az、time、timeStamp存入 目錄i中
                    dataRedUpdates.put("time", date);
                    dataRedUpdates.put("timeStamp", timeStamp);
                    dataRedUpdates.put("ax", ax);
                    dataRedUpdates.put("ay", ay);
                    dataRedUpdates.put("az", az);
                    childRef.updateChildren(dataRedUpdates);
                }

                // 傳送到手機
                if(action.equals("mobile") || action.equals("both")){
                    String message = date + "," + ax + "," + ay + "," + az;

                    //Sending a message can block the main UI thread, so use a new thread//
                    if(hand.equals("left")){
                        new NewThread("/accelerometer_left", message).start();
                    }
                    else if(hand.equals("right")){
                        new NewThread("/accelerometer_right", message).start();
                    }
                }
            }
        }
        //gyroscope
        else if (sensorEvent.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
            if((System.currentTimeMillis() - lastSaved_gyroscope) >= ACCE_FILTER_DATA_MIN_TIME) {
                //時間戳 timeStamp
                long timeStamp = sensorEvent.timestamp;
                long timeInMillis = (System.currentTimeMillis()
                        + (timeStamp - SystemClock.elapsedRealtimeNanos()) / 1000000L);
                //將時間戳轉為日期
                String date=stampToDate(timeInMillis);

                lastSaved_gyroscope = System.currentTimeMillis();
                float gx = sensorEvent.values[0];
                float gy = sensorEvent.values[1];
                float gz = sensorEvent.values[2];

                Log.d(TAG, "onSensorChanged_gyroscope: gX:" + gx + " gY: " + gy + " gZ: " + gz + " " + date);

                //傳送到firebase
                if(action.equals("firebase") || action.equals("both")){
                    DatabaseReference childRef = dateRef.child(String.valueOf(i));
                    Map<String, Object> dataRedUpdates = new HashMap<>();
                    //將gx,gy,gz跟存入 目錄i中
                    dataRedUpdates.put("gx",gx);
                    dataRedUpdates.put("gy",gy);
                    dataRedUpdates.put("gz",gz);
                    childRef.updateChildren(dataRedUpdates);
                    i++;
                }

                // 傳送到手機
                if(action.equals("mobile") || action.equals("both")){
                    String message = date + "," + gx + "," + gy + "," + gz;
                    //Sending a message can block the main UI thread, so use a new thread//
                    if(hand.equals("left")){
                        new NewThread("/gyroscope_left", message).start();
                    }
                    else if(hand.equals("right")){
                        new NewThread("/gyroscope_right", message).start();
                    }
                }
            }
        }

    }

    // 將時間戳轉為日期格式
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private String stampToDate(long timeInMillis){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date date = new Date(timeInMillis);
        return simpleDateFormat.format(date);
    }

    //獲取裝置id
    private String getSerialNumber() {
        String uniqueID = "";
        try {
            uniqueID = Build.getSerial();
            Log.d(TAG, "手錶序號: " + uniqueID);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "無法取得手錶序號");
            uniqueID="unknown";
        }
        return uniqueID;

    }

    // 傳送訊息
    class NewThread extends Thread {
        String path;
        String message;

        //Constructor for sending information to the Data Layer//
        NewThread(String p, String m) {
            path = p;
            message = m;
        }

        public void run() {
            //Retrieve the connected devices, known as nodes//
            Task<List<Node>> wearableList =
                    Wearable.getNodeClient(getApplicationContext()).getConnectedNodes();
            try {
                List<Node> nodes = Tasks.await(wearableList);
                for (Node node : nodes) {
                    mConnectedNode = node.getId();
                    Log.d(TAG, "The connected node is " + mConnectedNode);
                    Task<Integer> sendMessageTask =
                            //Send the message//
                            Wearable.getMessageClient(SensorService.this).sendMessage(mConnectedNode, path, message.getBytes());
                    try {
                        //Block on a task and get the result synchronously//
                        Integer result = Tasks.await(sendMessageTask);
                        Log.d(TAG, "Message sent: " + result);
                    } catch (ExecutionException exception) {
                        Log.e(TAG, "Task failed: " + exception);
                    } catch (InterruptedException exception) {
                        Log.e(TAG, "Interrupt occurred: " + exception);
                    }

                }
            } catch (ExecutionException exception) {
                Log.e(TAG, "Task failed: " + exception);
            } catch (InterruptedException exception) {
                Log.e(TAG, "Interrupt occurred: " + exception);
            }

        }
    }

    //service啟動時創建一條通知，與其綁定，這樣鎖屏或後台服務都不會被暫停或創建。
    private NotificationManager notificationManager = null;
    private String notificationId = "1000";
    private String notificationName = "SensorService";
    private void showNotification(){
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //創建NotificationChannel
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channel = new NotificationChannel(notificationId, notificationName, NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
        }
        startForeground(1,getNotification());
    }
    private Notification getNotification() {
        Intent intent = new Intent(this, SensorActivity.class);
        Notification.Builder builder = new Notification.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Strokare")
                .setContentText("運作中~輕觸我以關閉")
                .setContentIntent(PendingIntent.getActivity(this, 0, intent, 0)); // 设置PendingIntent;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(notificationId);
        }
        Notification notification = builder.build();
        return notification;
    }

    // 讓其他class可以取得isStart
    public static boolean getIsStart(){
        return isStart;
    }
}