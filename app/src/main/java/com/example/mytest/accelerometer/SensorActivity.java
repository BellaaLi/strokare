package com.example.mytest.accelerometer;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import android.util.Log;
import android.view.Gravity;
import android.widget.Button;
import android.widget.TextView;

public class SensorActivity extends AppCompatActivity {

    private static final String TAG = "SensorActivity";
    private Button start_btn;
    private Button stop_btn;
    private static TextView textView;
    private static boolean isStart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_sensor);
        start_btn = (Button) findViewById(R.id.start_btn);
        stop_btn = (Button) findViewById(R.id.stop_btn);
        textView = (TextView) findViewById(R.id.value);
        start_btn.setBackgroundColor(Color.GRAY);
        stop_btn.setBackgroundColor(Color.GRAY);

        // 得知目前service是否開啟
        isStart = SensorService.getIsStart();

        // 獲取手機權限
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 101);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        Intent intent = new Intent(getApplicationContext(), SensorService.class);

        checkStart();
        // 設置按下start按鈕後的動作(開啟service)
        start_btn.setOnClickListener(v -> {

            //建立dialog_選擇配戴手
            final String[] hand = {"左手","右手"};
            AlertDialog.Builder dialog_hand = new AlertDialog.Builder(this);
            // Customise Title
            TextView title = new TextView(this);
            title.setText("配戴於");
            title.setPadding(10, 10, 10, 10);
            title.setGravity(Gravity.CENTER);
            title.setTextColor(Color.WHITE);
            title.setTextSize(30);
            dialog_hand.setCustomTitle(title);

            //只要你在onClick處理事件內，使用which參數，就可以知道按下陣列裡的哪一個了
            dialog_hand.setItems(hand, (dialog, which) -> {
                switch (which){
                    case 0:
                        intent.putExtra("hand","left");
                        break;
                    case 1:
                        intent.putExtra("hand","right");
                        break;
                    default:
                        break;
                }

                //建立dialog_選擇執行動作
                final String[] action = {"傳送到firebase","傳送到手機","以上皆是"};
                AlertDialog.Builder dialog_action = new AlertDialog.Builder(this);
                // Customise Title
                TextView title_action = new TextView(this);
                title_action.setText("執行");
                title_action.setPadding(10, 10, 10, 10);
                title_action.setGravity(Gravity.CENTER);
                title_action.setTextColor(Color.WHITE);
                title_action.setTextSize(30);
                dialog_action.setCustomTitle(title_action);
                dialog_action.setItems(action, (dialog2, which2) -> {
                    switch (which2){
                        case 0:
                            intent.putExtra("action","firebase");
                            break;
                        case 1:
                            intent.putExtra("action","mobile");
                            break;
                        default:
                            intent.putExtra("action","both");
                            break;
                    }


                    //執行service
                    startService(intent);
                    isStart = true;
                    checkStart();
                });
                dialog_action.show();

            });
            dialog_hand.show();

        });

        // 設置按下stop按鈕後的動作(停止service)
        stop_btn.setOnClickListener(v -> {
            stopService(intent);
            isStart = false;
            checkStart();
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    //檢查service有無被啟動過了，並使得按鈕有相應的狀態
    private void checkStart() {
        if (isStart) {
            start_btn.setEnabled(false);
            stop_btn.setEnabled(true);
            textView.setText("蒐集數據中(｡･∀･)ﾉﾞ");
        } else {
            start_btn.setEnabled(true);
            stop_btn.setEnabled(false);
            textView.setText("已停止偵測!!!");
        }
    }


}